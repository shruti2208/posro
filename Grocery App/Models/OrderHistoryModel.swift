// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let orderHistoryModel = try? newJSONDecoder().decode(OrderHistoryModel.self, from: jsonData)

import Foundation

// MARK: - OrderHistoryModel
struct OrderHistoryModel: Codable {
    let status: Bool
    let code: Int
    let data: [OHData]
}

// MARK: - Datum
struct OHData: Codable {
    let orderID, orderNo, orderDate, orderTotal: String
    let orderDiscount, orderCouponcodeAmount, orderGst, orderDelCharges: String
    let orderStatus, orderCompleted, orderFeedback: String
    let products: [OHProduct]
    let timeslot: [Timeslot]

    enum CodingKeys: String, CodingKey {
        case orderID = "order_id"
        case orderNo = "order_no"
        case orderDate = "order_date"
        case orderTotal = "order_total"
        case orderDiscount = "order_discount"
        case orderCouponcodeAmount = "order_couponcode_amount"
        case orderGst = "order_gst"
        case orderDelCharges = "order_del_charges"
        case orderStatus = "order_status"
        case orderCompleted = "order_completed"
        case orderFeedback = "order_feedback"
        case products, timeslot
    }
}

// MARK: - Product
struct OHProduct: Codable {
    let totalitems: String
}

// MARK: - Timeslot
struct Timeslot: Codable {
    let id, timeFrom, timeTo: String

    enum CodingKeys: String, CodingKey {
        case id
        case timeFrom = "time_from"
        case timeTo = "time_to"
    }
}

