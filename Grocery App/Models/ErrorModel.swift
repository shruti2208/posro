//
//  ErrorModel.swift
//  Goan Cart - Vendor
//
//  Created by Shruti Gawas on 23/04/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import Foundation
struct ErrorModel: Codable {
    let code: Int
    let msg, type, error: String?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case msg = "Msg"
        case type = "Type"
        case error = "Error"
    }
}
struct OtpModel: Codable {
    let status: Bool
    let code: Int
    let data, message: String
}
struct VerifiedOtpModel: Codable {
    let status: Bool
    let code: Int
    let data: String
}
struct AddedToCartModel: Codable {
    let status: Bool?
    let code, counter: Int?
    let data: [DD]
}

// MARK: - Datum
struct DD: Codable {
    let productID: String?
    let check: [String: Int]

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case check
    }
}

// MARK: - Datum
struct Dat: Codable {
    let productID: Int?
    let check: [String: Int]

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case check
    }
}
struct CannotAddProductModel: Codable {
    let status: Bool
    let code, counter: Int
    let data: [FailData]
}

// MARK: - Datum
struct FailData: Codable {
    let productID: String?
    let check: Check

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case check
    }
}

// MARK: - Check
struct Check: Codable {
    let exists, qty: Int
    let msg: String
    let productID : String?

    enum CodingKeys: String, CodingKey {
        case exists, qty
        case productID = "product_id"
        case msg
    }
}

struct CouponValidityModel: Codable {
    let status: Bool
    let code: Int
    let data: CouponData
}

// MARK: - DataClass
struct CouponData: Codable {
    let discount : String?
    let message: String
}
struct VerifyStockModel: Codable {
    let status: Bool
    let code: Int
    let msg: String
}
struct DelAdd: Codable {
    let status: Bool
    let code: Int
    let data: Bool
}
struct AddressAddedModel: Codable {
    let status: Bool
    let code, data: Int
}
struct OrderPlacedModel: Codable {
    let status: Bool
    let code: Int
    let msg: String
    let orderID: Int

    enum CodingKeys: String, CodingKey {
        case status, code, msg
        case orderID = "order_id"
    }
}
struct UpdatedContactErrorModel: Codable {
    let status: Bool
    let code: Int
    let data: String
}
struct GenericErrorModel: Codable {
    let status: Bool
    let code: Int
}
// MARK: - GenericError
struct GenericError: Codable {
    let status: Bool
    let code: Int
    let message: String
}
// MARK: - SignedUpModel
struct SignedUpModel: Codable {
    let status: Bool
    let code: Int
    let data: SignUpData
    let message: String
}

// MARK: - DataClass
struct SignUpData: Codable {
    let id, name, email, contact: String
    let cartCount: String

    enum CodingKeys: String, CodingKey {
        case id, name, email, contact
        case cartCount = "cart_count"
    }
}

// MARK: - InvalidCoupon
struct InvalidCoupon: Codable {
    let status: Bool
    let code: Int
    let data: InvDate
}

// MARK: - DataClass
struct InvDate: Codable {
    let discount: Int
    let message: String
}
// MARK: - ContactUsModel
struct ContactUsModel: Codable {
    let status: Bool
    let code: Int
    let data: cu
}

// MARK: - DataClass
struct cu: Codable {
    let contact, email: String
}
struct ListAddedModel: Codable {
    let status: Bool
    let code: Int
    let data: Bool
}
