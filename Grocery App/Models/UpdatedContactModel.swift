// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let updatedContactModel = try? newJSONDecoder().decode(UpdatedContactModel.self, from: jsonData)

import Foundation

// MARK: - UpdatedContactModel
struct UpdatedContactModel: Codable {
    let status: Bool
    let code: Int
    let data: DataClasss
}

// MARK: - DataClass
struct DataClasss: Codable {
    let id, name, email, password: String
    let contact, otpVerified, activeAddress, dateCreated: String
    let lastLogin, userType, status, loyaltyPointsTotal: String
    let blocked: String

    enum CodingKeys: String, CodingKey {
        case id, name, email, password, contact
        case otpVerified = "otp_verified"
        case activeAddress = "active_address"
        case dateCreated = "date_created"
        case lastLogin = "last_login"
        case userType = "user_type"
        case status
        case loyaltyPointsTotal = "loyalty_points_total"
        case blocked
    }
}
