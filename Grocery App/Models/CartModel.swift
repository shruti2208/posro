// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let cartModel = try? newJSONDecoder().decode(CartModel.self, from: jsonData)

import Foundation

// MARK: - CartModel
struct CartModel: Codable {
    let status: Bool?
    let code: Int?
    let coDcode, payonlinecode: Bool?
    let msg: String?
    var summary: Summary
    let data: DataClass1?

    enum CodingKeys: String, CodingKey {
        case status, code
        case coDcode = "CODcode"
        case payonlinecode, msg, summary, data
    }
}

// MARK: - DataClass
struct DataClass1: Codable {
    let count: Int?
    let pageNo: String?
    let totalPages: Int?
    var products: [Product1]?

    enum CodingKeys: String, CodingKey {
        case count
        case pageNo = "page_no"
        case totalPages = "total_pages"
        case products
    }
}

// MARK: - Product
struct Product1: Codable {
    var id, userID, productID, quantity: String?
    var discount, dateCreated, categoryID, name: String?
    var localName, price, taxID, cgst: String?
    var sgst, quantityAvailable, pickOrder, quantityType: String?
    var weightUnit, pieceUnit, image, status: String?
    var percent: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case productID = "product_id"
        case quantity, discount
        case dateCreated = "date_created"
        case categoryID = "category_id"
        case name
        case localName = "local_name"
        case price
        case taxID = "tax_id"
        case cgst, sgst
        case quantityAvailable = "quantity_available"
        case pickOrder = "pick_order"
        case quantityType = "quantity_type"
        case weightUnit = "weight_unit"
        case pieceUnit = "piece_unit"
        case image, status, percent
    }
}

// MARK: - Summary
struct Summary: Codable {
    var totalDiscount, subTotal, totalGst, deliveryLimit: String?
    var deliveryAmt, minOrderAmt, deliveryCharges, grandTotal: String?
    var validOrder, netTotal: String?

    enum CodingKeys: String, CodingKey {
        case totalDiscount = "total_discount"
        case subTotal = "sub_total"
        case totalGst = "total_gst"
        case deliveryLimit = "delivery_limit"
        case deliveryAmt = "delivery_amt"
        case minOrderAmt = "min_order_amt"
        case deliveryCharges = "delivery_charges"
        case grandTotal = "grand_total"
        case validOrder = "valid_order"
        case netTotal = "net_total"
    }
}



