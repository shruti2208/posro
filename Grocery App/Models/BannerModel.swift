// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let bannerModel = try? newJSONDecoder().decode(BannerModel.self, from: jsonData)

import Foundation

// MARK: - BannerModel
struct BannerModel: Codable {
    let status: Bool
    let code: Int
    let data: [Datum1]
}

// MARK: - Datum
struct Datum1: Codable {
    let id, categoryID, offerID, image: String
    let link, visible, displayText: String

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case offerID = "offer_id"
        case image, link, visible
        case displayText = "display_text"
    }
}

