// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginModel = try? newJSONDecoder().decode(LoginModel.self, from: jsonData)

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let status: Bool
    let code: Int
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let id, name, email, contact: String
    let cartCount: String

    enum CodingKeys: String, CodingKey {
        case id, name, email, contact
        case cartCount = "cart_count"
    }
}


//struct SignedUpModel: Codable {
//    let status: Bool
//    let code: Int
//    let data: SignUpData
//    let message: String
//}
//
//// MARK: - DataClass
//struct SignUpData: Codable {
//    let id, name, email, contact: String
//    let cartCount: String
//
//    enum CodingKeys: String, CodingKey {
//        case id, name, email, contact
//        case cartCount = "cart_count"
//    }
//}
