// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let offersModel = try? newJSONDecoder().decode(OffersModel.self, from: jsonData)

import Foundation

// MARK: - OffersModel
struct OffersModel: Codable {
    let status: Bool?
    let code: Int?
    let data: DataClas?
}

// MARK: - DataClass
struct DataClas: Codable {
    let pageNo: String?
    let totalPages: Int?
    let products: [Product]

    enum CodingKeys: String, CodingKey {
        case pageNo = "page_no"
        case totalPages = "total_pages"
        case products
    }
}

// MARK: - Product
struct Product: Codable {
    let id, categoryID, name, localName: String?
    let price, taxID, cgst, sgst: String?
    let quantityAvailable, pickOrder, quantityType, weightUnit: String?
    let pieceUnit, image, status, discount: String?
    let quantityAdded: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case name
        case localName = "local_name"
        case price
        case taxID = "tax_id"
        case cgst, sgst
        case quantityAvailable = "quantity_available"
        case pickOrder = "pick_order"
        case quantityType = "quantity_type"
        case weightUnit = "weight_unit"
        case pieceUnit = "piece_unit"
        case image, status, discount
        case quantityAdded = "quantity_added"
    }
}

