// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let orderDetailsModel = try? newJSONDecoder().decode(OrderDetailsModel.self, from: jsonData)

import Foundation

// MARK: - OrderDetailsModel
struct OrderDetailsModel: Codable {
    let status: Bool
    let code: Int
    let data: ODDataClass
}

// MARK: - DataClass
struct ODDataClass: Codable {
    let pageNo: String
    let totalPages: Int
    let orderID, orderNo, orderDate, orderTotal: String
    let orderDelCharges, orderDiscount, orderGst, orderSubtotal: String
    let orderCouponcodeAmount, orderStatus, orderCompleted, orderDeliveryText: String
    let orderName, orderMobile, orderPincode, orderAddress: String
    let orderCity, orderLocality, orderLandmark, orderPaymentType: String
    let orderPaymentID: String
    let products: [ODProduct]

    enum CodingKeys: String, CodingKey {
        case pageNo = "page_no"
        case totalPages = "total_pages"
        case orderID = "order_id"
        case orderNo = "order_no"
        case orderDate = "order_date"
        case orderTotal = "order_total"
        case orderDelCharges = "order_del_charges"
        case orderDiscount = "order_discount"
        case orderGst = "order_gst"
        case orderSubtotal = "order_subtotal"
        case orderCouponcodeAmount = "order_couponcode_amount"
        case orderStatus = "order_status"
        case orderCompleted = "order_completed"
        case orderDeliveryText = "order_delivery_text"
        case orderName = "order_name"
        case orderMobile = "order_mobile"
        case orderPincode = "order_pincode"
        case orderAddress = "order_address"
        case orderCity = "order_city"
        case orderLocality = "order_locality"
        case orderLandmark = "order_landmark"
        case orderPaymentType = "order_payment_type"
        case orderPaymentID = "order_payment_id"
        case products
    }
}

// MARK: - Product
struct ODProduct: Codable {
    let id, name, image, cgst: String
    let sgst, quantity, weightUnit, pieceUnit: String
    let quantityType, price, cgstAmount, sgstAmount: String
    let discount: String

    enum CodingKeys: String, CodingKey {
        case id, name, image, cgst, sgst, quantity
        case weightUnit = "weight_unit"
        case pieceUnit = "piece_unit"
        case quantityType = "quantity_type"
        case price
        case cgstAmount = "cgst_amount"
        case sgstAmount = "sgst_amount"
        case discount
    }
}

