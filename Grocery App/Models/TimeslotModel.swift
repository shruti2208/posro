// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let timeslotModel = try? newJSONDecoder().decode(TimeslotModel.self, from: jsonData)

import Foundation

// MARK: - TimeslotModel
struct TimeslotModel: Codable {
    let status: Bool
    let code: Int
    let data: [time]
}

// MARK: - Datum
struct time: Codable {
    let id, name, timeFrom, timeTo: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case timeFrom = "time_from"
        case timeTo = "time_to"
    }
}

