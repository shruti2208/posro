
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let allCategoriesModel = try? newJSONDecoder().decode(AllCategoriesModel.self, from: jsonData)

import Foundation

// MARK: - AllCategoriesModel
struct AllCategoriesModel: Codable {
    let status: Bool
    let code: Int
    let data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let id, name, priority, image: String
    let icon: String
}
