// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let savedAddressesModel = try? newJSONDecoder().decode(SavedAddressesModel.self, from: jsonData)

import Foundation

// MARK: - SavedAddressesModel
struct SavedAddressesModel: Codable {
    let status: Bool
    let code: Int
    let data: [Add]
}

// MARK: - Datum
struct Add: Codable {
    let id, cityID, city, pincode: String
    let userID, name, mobile, address: String
    let lat, lon, locality, landmark: String
    let dateCreated, status: String

    enum CodingKeys: String, CodingKey {
        case id
        case cityID = "city_id"
        case city, pincode
        case userID = "user_id"
        case name, mobile, address, lat, lon, locality, landmark
        case dateCreated = "date_created"
        case status
    }
}

