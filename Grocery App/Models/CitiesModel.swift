// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let citiesModel = try? newJSONDecoder().decode(CitiesModel.self, from: jsonData)

import Foundation

// MARK: - CitiesModel
struct CitiesModel: Codable {
    let status: Bool
    let code: Int
    let data: [Cities]
}

// MARK: - Datum
struct Cities: Codable {
    let cityID, cityName, cityPin, cityDeliveryCharges: String
    let visible: String

    enum CodingKeys: String, CodingKey {
        case cityID = "city_id"
        case cityName = "city_name"
        case cityPin = "city_pin"
        case cityDeliveryCharges = "city_delivery_charges"
        case visible
    }
}

