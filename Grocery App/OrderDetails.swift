//
//  OrderDetails.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class OrderDetails: UIViewController {
    var addressObj : Add!
    var cartObj : CartModel!
    var timeSlotId : String!
    var delDate : String!
    @IBOutlet var addresstype: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var landmark: UILabel!
    @IBOutlet var ph: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var btmView: UIView!
    
    
    @IBOutlet var lblSubtotal: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDelCharges: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblGst: UILabel!
    var grandTotal : Double!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        addresstype.text = addressObj.name
        address.text = addressObj.address
        landmark.text = addressObj.landmark
        ph.text = addressObj.mobile
        
        topView.borderColor = Constants.AppColor.themeColorTurquoise
        btmView.borderColor = Constants.AppColor.themeColorTurquoise
        
        topView.borderWidth = 1.0
        btmView.borderWidth = 1.0
        
//        lblSubtotal.text = cartObj.summary.subTotal
//        lblDiscount.text = cartObj.summary.totalDiscount
//        lblDelCharges.text = cartObj.summary.deliveryCharges
//        lblTotal.text = cartObj.summary.grandTotal
//        lblGst.text = cartObj.summary.totalGst
        
        
              let subTotal = Double(cartObj.summary.subTotal!)
              let totalDiscount = Double(cartObj.summary.totalDiscount!)
              let deliveryCharges = Double(cartObj.summary.deliveryCharges!)
              let totalGst = Double(cartObj.summary.totalGst!)

              grandTotal = subTotal! + totalDiscount! + deliveryCharges! + totalGst!
              cartObj.summary.grandTotal = String(grandTotal)
              lblSubtotal.text = "₹ " + String(format: "%.2f",subTotal!)
              lblDiscount.text = "₹ " + String(format: "%.2f",totalDiscount!)
              lblDelCharges.text = "₹ " + String(format: "%.2f",deliveryCharges!)
              lblTotal.text = "₹ " + String(format: "%.2f",grandTotal)
              lblGst.text = "₹ " + String(format: "%.2f",totalGst!)
    }
    @IBAction func placeOrder(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "user_id=\(addressObj.userID)&address_id=\(addressObj.id)&city_id=\(addressObj.cityID)&offer_id=0&total_amount=\(cartObj!.summary.grandTotal!)&discount=\(cartObj!.summary.totalDiscount!)&timeslot_id=\(timeSlotId!)&delivery_date=\(delDate!)&coupon_code=&coupon_code_amount=0.0&gst=\(cartObj!.summary.totalGst!)&sub_total=\(cartObj!.summary.subTotal!)&payment_type=C&API_KEY=FabCodersENCRYPT"
        
        
       

        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.place_order, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: OrderPlacedModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"OrderPlaced") as! OrderPlaced
                    destinationVC.orderId = String(model.orderID)
                    destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.show(destinationVC, sender: self)
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
   
}
