//
//  AppDelegate.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import IQKeyboardManager
import DropDown
import GoogleMaps
import GooglePlaces
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()

        var navigationBarAppearace = UINavigationBar.appearance()

        navigationBarAppearace.tintColor = UIColor.init(hex: "ffffff")
//        navigationBarAppearace.barTintColor = UIColor.init(hex:"262424")
        navigationBarAppearace.barTintColor = UIColor.init(hex:"000000")

        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        GMSServices.provideAPIKey("AIzaSyA0KeL9H2GzitlvCLmcRZ5nHUg5yRJnYdg")
        GMSPlacesClient.provideAPIKey("AIzaSyA0KeL9H2GzitlvCLmcRZ5nHUg5yRJnYdg")
        
        DropDown.startListeningToKeyboard()

        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController : UIViewController!

        let loggedIn = UserDefaults.standard.bool(forKey: "loggedIn")
        if loggedIn == true {
            initialViewController = storyboard.instantiateViewController(withIdentifier: "NavController") as? NavController
//            initialViewController = storyboard.instantiateViewController(withIdentifier: "Login") as? Login

        }
        else{
            initialViewController = storyboard.instantiateViewController(withIdentifier: "FirstNav") as? FirstNav

        }
        self.window?.rootViewController = initialViewController
        //               }
        self.window?.makeKeyAndVisible()

        return true
    }
    
    
    
    
}

