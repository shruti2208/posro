//
//  Utility.swift
//  BankingApp
//
//  Created by Nitin on 26/07/19.
//  Copyright © 2019 Nitin. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import SKActivityIndicatorView
class Utility {
    
    enum PhotoError: Error {
        case ConvertToData
        case PhotoDecoding
        case downloadPhotoUrl
        case downloadPhotoConvertToData
        case downloadPhotoConvertToUIImage
    }
   class func getTopMostViewController() -> UIViewController? {
         if var topController = UIApplication.shared.keyWindow?.rootViewController {
             while let presentedViewController = topController.presentedViewController {
                 topController = presentedViewController
             }
             return topController
         }
         return nil
     }
    class func generateParam(withObject data: [String: Any]) -> [String: Any] {
        
        return Utility.generateParameters(withObject: data)
        
    }
    
    
    private class func generateParameters(withObject data: [String: Any]) -> [String: Any] {
        //App/Device constants
        
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: [])
        
        let strMessage = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        print(strMessage)
        
        
        //Convert Encrypted Response of Hex to Base 64
        //        let binaryData = Data(hexString: strMessage)
        //        let base64Param = binaryData!.base64EncodedString()
        //
        //        let param = ["data": base64Param]
        let param = ["data": strMessage]
        
        return param
    }
    class func generateParametersNew(withObject data: [String: Any]) -> String {
        //App/Device constants
        
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: [])
        
        let strMessage = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        print(strMessage)
        
        
        //Convert Encrypted Response of Hex to Base 64
        //        let binaryData = Data(hexString: strMessage)
        //        let base64Param = binaryData!.base64EncodedString()
        //
        //        let param = ["data": base64Param]
        //         let param = ["data": strMessage]
        
        return strMessage
        
    }
    
    /// Decrypt Data
    ///
    /// - Parameter data: Data to be encrypted
    ///
    /// - Returns: Calling Class Type
    class func decode<T> (data: Data) -> T? where T:Decodable{
        
        do {
            return try JSONDecoder().decode(T.self, from: data)
        }
        catch {
            
        }
        return nil
        
    }
    class func decodeData<T> (data: Data) -> T? where T:Decodable {
        
        if data.isEmpty {
            return nil
        }
        
        do {
            return try JSONDecoder().decode(T.self, from: data)
        }
        catch {
            
        }
        return nil
    }
    
    class func handleNetworkError (error: WSError) {
        let msg = "Download Failed! \n\(error.msg). \n Code \(error.code)"
        
        print(msg)
        
        DispatchQueue.main.async(execute: {
            if let topVC = UIApplication.topViewController() {
                topVC.alert(message: msg)
            }
        })
    }
    func showErrorAlert(_ message: String) {
        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func downloadPhotos(photos: [String],folderName:String) -> Promise<[UIImage]> {
        return Promise { seal in
            
            var count = 0
            for photo in photos {
                guard let photoUrl = URL(string: photo) else {
                    seal.reject(PhotoError.downloadPhotoUrl)
                    return
                }
                guard let photoData = try? Data(contentsOf: photoUrl) else {
                    seal.reject(PhotoError.downloadPhotoConvertToData)
                    return
                }
                guard let photoImage = UIImage(data: photoData) else {
                    seal.reject(PhotoError.downloadPhotoConvertToUIImage)
                    return
                }
                //                self.photos.append(photoImage)
                self.saveImageToDocumentDirectory(chosenImage:photoImage,name:photoUrl.lastPathComponent,folderName : folderName)
                count+=1
                print("Finished downloading photo: " + String(count))
                
            }
            //            print("Finished downloading \(self.photos.count) photos")
            //            seal.fulfill(self.photos)
        }
    }
    class func saveImageToDocumentDirectory(chosenImage: UIImage,name : String!,folderName : String) -> String {
        //        let directoryPath =  NSHomeDirectory().appending("/Documents/")
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let docURL = URL(string: documentsDirectory)!
        let dataPath = docURL.appendingPathComponent(folderName)
        
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription);
            }
        }
        
        
        
        //        let filename = dateFormatter.string(from: Date()).appending(".jpg")
        let pathPrefix = (name as NSString).deletingPathExtension   // File name without extension
        
        let filename = pathPrefix + ".png"
        let filepath = dataPath.appendingPathComponent(filename)
        let url = NSURL.fileURL(withPath: filepath.absoluteString)
        do {
            try chosenImage.jpegData(compressionQuality: 1.0)?.write(to: url, options: .atomic)
            print(dataPath.absoluteString)
            return String.init("/Documents/\(folderName)/\(filename)")
            
        } catch {
            print(error)
            print("file cant not be save at path \(filepath), with error : \(error)");
            return filepath.absoluteString
        }
    }
    class func downloadPhotos(photos: [String], withName:String) -> Promise<[UIImage]> {
        return Promise { seal in
            
            var count = 0
            for photo in photos {
                guard let photoUrl = URL(string: photo) else {
                    seal.reject(PhotoError.downloadPhotoUrl)
                    return
                }
                guard let photoData = try? Data(contentsOf: photoUrl) else {
                    seal.reject(PhotoError.downloadPhotoConvertToData)
                    return
                }
                guard let photoImage = UIImage(data: photoData) else {
                    seal.reject(PhotoError.downloadPhotoConvertToUIImage)
                    return
                }
                //                self.photos.append(photoImage)
                
                self.saveImageToDocumentDirectory(photoImage, withName: withName)
                count+=1
                print("Finished downloading photo: " + String(count))
                
            }
            //            print("Finished downloading \(self.photos.count) photos")
            //            seal.fulfill(self.photos)
        }
        
    }
    class func saveImageToDocumentDirectory(_ chosenImage: UIImage, withName:String) -> String {
        let directoryPath =  NSHomeDirectory().appending("/Documents/")
        if !FileManager.default.fileExists(atPath: directoryPath) {
            do {
                try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: directoryPath), withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error)
            }
        }
        
        
        let filename = withName.appending(".jpg")
        let filepath = directoryPath.appending(filename)
        let url = NSURL.fileURL(withPath: filepath)
        do {
            try chosenImage.jpegData(compressionQuality: 1.0)?.write(to: url, options: .atomic)
            print(directoryPath)
            return String.init("/Documents/\(filename)")
            
        } catch {
            print(error)
            print("file cant not be save at path \(filepath), with error : \(error)");
            return filepath
        }
    }
    class func validatePhoneNumber(phoneNumber: String) -> Bool {
        //        let phoneNumberRegex = "^[0-9]{10}$" //(@"^[0-9]{10}$")
        
        let PHONE_REGEX1 = "^[0-9]{10}$"
        let phoneTest1 = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX1)
        let result1 =  phoneTest1.evaluate(with: phoneNumber)
        return result1
        
        
        //        return result
    }
    class func validateEmailId(emailID: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let trimmedString = emailID.trimmingCharacters(in: .whitespaces)
        let validateEmail = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let isValidateEmail = validateEmail.evaluate(with: trimmedString)
        return isValidateEmail
    }
    class func validatePassword(password: String) -> Bool {
        //Minimum 8 characters at least 1 Alphabet and 1 Number,one special character:
        
        let passRegEx     = "^(?=.*[!@#$&*.])(?=.*[0-9])(?=.*[A-Za-z]).{8,}$"
        let trimmedString = password.trimmingCharacters(in: .whitespaces)
        let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let isvalidatePass = validatePassord.evaluate(with: trimmedString)
        return isvalidatePass
        
        
    }
    class func validateGST(gst: String) -> Bool {
        let gstRegEx = "^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$"
        let trimmedString = gst.trimmingCharacters(in: .whitespaces)
        let validateGst = NSPredicate(format:"SELF MATCHES %@", gstRegEx)
        let isValidateGst = validateGst.evaluate(with: trimmedString)
        return isValidateGst
    }
    class func validatePAN(pan: String) -> Bool {
        let panRegEx = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        let trimmedString = pan.trimmingCharacters(in: .whitespaces)
        let validatePan = NSPredicate(format:"SELF MATCHES %@", panRegEx)
        let isValidatePan = validatePan.evaluate(with: trimmedString)
        return isValidatePan
    }
    class func validateIFSC(ifsc: String) -> Bool {
        let ifscRegEx = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$"
        let trimmedString = ifsc.trimmingCharacters(in: .whitespaces)
        let validateifsc = NSPredicate(format:"SELF MATCHES %@", ifscRegEx)
        let isValidateifsc = validateifsc.evaluate(with: trimmedString)
        return isValidateifsc
    }
    
   class func showAlert(title : String) {
//        let margin = SCLAlertView.SCLAppearance.Margin(buttonSpacing: 30,
//                                                       bottom: 30,
//                                                       horizontal: 30)
//
//        let appearance = SCLAlertView.SCLAppearance(
//
//            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
//            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
//            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
//            showCloseButton: false,
//
//            shouldAutoDismiss: true,
//            margin: margin
//        )
//        let alert = SCLAlertView(appearance: appearance)
//        _ = alert.addButton("Ok"){
//            print("Ok button tapped")
//        }
//
//        let color = Constants.AppColor.themeColorTangerine
//
//        _ = alert.showCustom(title, subTitle: "", color: color)
        
    }
   class func convertIntoJSONString(arrayObject:[Any]) -> String? {
           
           do {
               let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
               if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                   return jsonString as String
               }
               
           } catch let error as NSError {
               print("Array convertIntoJSON - \(error.description)")
           }
           return nil
       }
    
}


let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        self.image = nil

        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }

        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                }
            }

        }).resume()
    }
}
