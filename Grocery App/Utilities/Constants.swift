//
//  Constant.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    
    
    static let maxAccountNumber = 20
    static let minAccountNumber = 9
    static let maxIFSC = 11
    struct Networking {
        
        static let baseUrl      = ""
        
    }
    
    
    struct EndPoint {
        
    }
    
    
    
    struct AppIdentity {
        
        static let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
        static let appBundleId = Bundle.main.bundleIdentifier!
        static let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        static let appBuildVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
    }
    
    struct AppColor {
//        static let themeColor = UIColor.init(hex: "#9908AA")
        static let themeColor = UIColor.init(hex: "#0D839F")

        
        static let themeColorGreen = UIColor.init(hex: "#A7CE39")
        static let themeColorTurquoise = UIColor.init(hex: "#0D839F")
        
        
        static let themeColorGray = UIColor.init(hex: "#231F20")
        
        static let themeNavTintColor = UIColor.white
        static let themeNavBarColor = UIColor.init(hex: "#24C355")
    }
    
    struct AppFont {
        static let navigationTitle = UIFont.systemFont(ofSize: 12)
        //        static let navigationButton = UIFont.Font(FontName(rawValue: FontName.Roboto.rawValue)!, type: FontType.Bold, size: 17)
    }
    
    struct Size {
        static let statusBarHeight: CGFloat = 20
        static let navigationBarHeight: CGFloat = 64
        static let navigationBarWithoutStatusHeight = navigationBarHeight - statusBarHeight
        static let tabBarHeight: CGFloat = 49
        static let screenWidth = UIScreen.main.bounds.size.width
        static let screenHeight = UIScreen.main.bounds.size.height
        static let screenMaxLength = max(screenWidth, screenHeight)
        static let screenMinLength = min(screenWidth, screenHeight)
    }
    
    struct Shared {
        static let sharedApplication    = UIApplication.shared
        static let applicationDelegate  = sharedApplication.delegate as! AppDelegate
        static let userDefaults         = UserDefaults.standard
        static let notificationCenter   = NotificationCenter.default
        static let mainBundle           = Bundle.main
        static let mainScreen           = UIScreen.main
    }
    
    struct Device {
        static let isIpad = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
        static let isIphone = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
        static let isTV = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.tv
        static let isIphone4 = isIphone && Constants.Size.screenMaxLength < 568.0
        static let isIphone5 = isIphone && Constants.Size.screenMaxLength == 568.0
        static let isIphone6 = isIphone && Constants.Size.screenMaxLength == 667.0
        static let isIphone6P = isIphone && Constants.Size.screenMaxLength == 736.0
        static let isIphoneX = isIphone && Constants.Size.screenMaxLength == 812.0
        static let serialNo = (UIDevice.current.identifierForVendor?.uuidString.replacingOccurrences(of: "-", with: ""))!
        
    }
    
    struct SystemVersion {
        static let iOSVersion = (UIDevice.current.systemVersion as NSString).floatValue
        let isIOS7 = iOSVersion <  8.0 && iOSVersion >= 7.0
        let isIOS8 = iOSVersion >= 8.0 && iOSVersion <  9.0
        let isIOS9 = iOSVersion >= 9.0 && iOSVersion <  10.0
        let isIOS10 = iOSVersion >= 10.0 && iOSVersion <  11.0
        let isIOS11 = iOSVersion >= 11.0 && iOSVersion <  12.0
        let isIOS12 = iOSVersion >= 12.0 && iOSVersion <  13.0
        
    }
    
    struct CellIdentifier {
        
    }
    
    struct NibIdentifier {
        
    }
    
    struct ControllerId {
        
    }
    
    struct ControllerSegue {
        
    }
    
    
}

struct APIEndpoint {
    
    
    //    static let base = "https://machotelslimited.com/farmfresh/api/"
    static let base = "https://machotelslimited.com/posro-demo/api/"
    static let baseImg = "https://machotelslimited.com/posro-demo/"
    
    
    static let login     = base + "User/login"
    static let send_otp     = base + "user/send_otp"
    static let verify_otp     = base + "user/verify_otp"
    static let get_all_categories     = base + "product/get_all_categories"
    static let get_banner     = base + "banner/get_banner"
    static let get_products_with_discount     = base + "product/get_products_with_discount"
    static let get_product_search     = base + "product/get_product_search"
    static let get_cart     = base + "cart/get_cart"
    static let update_cart_product     = base + "cart/update_cart_product"
    static let get_coupon_validity     = base + "coupon/get_coupon_validity"
    static let check_stock_available     = base + "Cart/check_stock_available"
    static let get_timeslot     = base + "timeslot/get_timeslot"
    static let get_user_addresses     = base + "user/get_user_addresses"
    static let delete_address     = base + "user/delete_address"
    static let add_edit_address     = base + "user/add_edit_address"
    static let get_all_city_info     = base + "user/get_all_city_info"
    static let get_order_history     = base + "user/get_order_history"
    static let place_order     = base + "order/place_order"
    static let update_user_profile     = base + "user/update_user_profile"
    static let get_products     = base + "product/get_products"
    static let get_order_details     = base + "user/get_order_details"
    static let order_cancellation     = base + "order/order_cancellation"
    static let remove_cart_product     = base + "cart/remove_cart_product"
    static let signup     = base + "user/signup"
    static let get_contact_info     = base + "config/get_contact_info"
    static let request_tex     = base + "Request/request_text"

    static let get_about     = base + "config/get_about?API_KEY=fab"
    static let get_help     = base + "config/get_help?API_KEY=fab"
    static let get_agreement     = base + "config/get_agreement?API_KEY=fab"
    //    static let get_about     = "http://goawebsites.com/sabziwala/api/config/get_about?API_KEY=fab"
    //    static let get_help     = "http://goawebsites.com/sabziwala/api/config/get_help?API_KEY=fab"
    //    static let get_agreement     = "http://goawebsites.com/sabziwala/api/config/get_agreement?API_KEY=fab"
    
    
    
    struct Request {
        let requestId = UUID().uuidString.replacingOccurrences(of: "-", with: "")
    }
    
}

