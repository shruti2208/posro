//
//  WSManager.swift
//  StarterProject
//
//  Created by Mayank Jain on 22/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit
import SKActivityIndicatorView

/// define error type
class WSError: NSObject {
    var msg:String
    var code:Int
    var type:WSErrorType
    
    /// init Eror object
    ///
    /// - Parameters:
    ///   - msg: error message
    ///   - code: error code
    ///   - type: error type (api failure response, server failed to respond)
    init(msg : String, code: Int, type:WSErrorType) {
        self.code = code
        self.msg = msg
        self.type = type
    }
}


/// define new result type
class WSResult: NSObject {
    var data:Data
//    var value:[String:Any]
    var value:AnyObject

    /// init result object
    ///
    /// - Parameters:
    ///   - data: in form of binary data
    ///   - value: key/value pair value
//    init(data : Data, value: [String:Any]) {
        init(data : Data, value: AnyObject) {

        self.data = data
        self.value = value
    }
}

/// http method
public enum WSHttpMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

/// error type
public enum WSErrorType: String {
    case server = "1"
    case api = "2"
}

// Completion Blocks
typealias wsSuccessBlock = (WSResult) -> Void

typealias wsFailureBlock = (WSError) -> Void

/// Error message
struct ErrorMsg {
    static let msgNoInternet       = "Internet is not connected"
    static let msgConnectionLost   = "Connection lost. Please check your network connectivity"
    static let msgServerError      = "Something went wrong, please try again"
    static let msgRequestTimeOut   = "Request time out, please try again"
    static let msgNoRecord         = "No record found"
    static let msgPageNotFound         = "Page Not Found"
}

class WSManager: NSObject {
    
    static let shared = WSManager() // Singleton
    
    /// setup url session
    let urlSession:URLSession = {
        let config = URLSessionConfiguration.default // Session Configuration
        //setup common header
        //        let headers = [
        //            "LoginSessionKey" : "abc@12345",
        //        ]
        //config.httpAdditionalHeaders = headers
        config.timeoutIntervalForRequest = 120.0
        config.timeoutIntervalForResource = 120.0
        
        let session = URLSession(configuration: config)// Load configuration into Session
        
        return session
    }()
    
    
    /// requestWSCall
    ///
    /// - Parameters:
    ///   - strWSName: wsName
    ///   - method: get,post,put etc
    ///   - params : `dict` of param
    ///   - isShowLoader: true/false
    ///   - success: success
    ///   - failure: failure
    
    func requestWSCall(strWSName: String,method:WSHttpMethod,params : [String : Any],isShowLoader:Bool, checksum:String, success:@escaping wsSuccessBlock, failure:@escaping wsFailureBlock) {
        
        
        if isShowLoader{
            //write code to show loader
            self.showNetworkActivity(show: true)
            
            SKActivityIndicator.spinnerStyle(.defaultSpinner)
            SKActivityIndicator.statusTextColor(Constants.AppColor.themeColor)
            SKActivityIndicator.show("Please wait...",userInteractionStatus: false)
            
        }
        
        //append WSName in base url
        var combineUrl = Constants.Networking.baseUrl + strWSName
        
        print("Fetching data for URL : \(combineUrl)")
        
        var urlRequest:URLRequest
        
        
        
        
        //if method is get append param in url
        if method == .get{
            combineUrl = combineUrl + params.queryString
            guard let url = NSURL(string:combineUrl) else {
                return
            }
            urlRequest = URLRequest(url: url as URL)
            
        }
            //else send param in http body
            
        else
        {
            guard let url = NSURL(string:combineUrl) else {
                return
            }
            
            urlRequest = URLRequest(url: url as URL)
            
            
            //setup request and response content type
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            //            urlRequest.addValue(Constants.Device.serialNo, forHTTPHeaderField: "deviceId")
            //            urlRequest.addValue(checksum, forHTTPHeaderField: "checkSum")
            urlRequest.cachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad

            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        urlRequest.httpMethod = method.rawValue
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            
            
            if isShowLoader{
                //write code to stop loader
                
                self.showNetworkActivity(show: false)
                SKActivityIndicator.dismiss()
                
            }
            
            guard (data != nil) || (error != nil) else {
                failure(WSError(msg: "No response received from server", code:0, type: .api))
                return
            }
            
            if (data?.isEmpty) == true{
                failure(WSError(msg: "No response received from server", code:0, type: .api))
                return
            }
            
            var valuePrint = [String:Any]()
            if method == WSHttpMethod.get {
                valuePrint["combineUrl"] = combineUrl
            }
            valuePrint["apiUrl"] = Constants.Networking.baseUrl + strWSName
            valuePrint["httpMethod"] = method
            valuePrint["param"] = params
            
            //            if (error != nil){
            //
            //                failure(WSError(msg: error!.localizedDescription, code:error!._code, type: .api))
            //                return
            //            }
            //            else{
            //
            //                let httpResponse = response as! HTTPURLResponse
            //
            //                if httpResponse.statusCode  == 404{
            //                    failure(WSError(msg: ErrorMsg.msgPageNotFound, code: httpResponse.statusCode, type: .server))
            //                    return
            //                }
            //
            //            }
            
            
            
            guard error == nil, let json = self.jsonSerializationWithData(data: data)
                else {
                    let code = (error! as NSError).code
                    var msg = ""
                    switch code{
                    case NSURLErrorTimedOut:
                        msg = ErrorMsg.msgRequestTimeOut
                    case NSURLErrorNotConnectedToInternet,NSURLErrorCannotConnectToHost:
                        msg = ErrorMsg.msgNoInternet
                    case NSURLErrorNetworkConnectionLost:
                        msg = ErrorMsg.msgConnectionLost
                    default:
                        msg = ErrorMsg.msgServerError
                    }
                    
                    valuePrint["error"] = WSError(msg: msg, code: code, type: .server)
                    valuePrint["response"] = nil
                    debugPrint(valuePrint)
                    
                    failure(WSError(msg: msg, code: code, type: .server))
                    return
            }
            
            valuePrint["response"] = json
            valuePrint["error"] = nil
            debugPrint(valuePrint)
            
            success(WSResult(data: data!, value: json))
            return
            
        }
        
        task.resume()
    }
    func requestWSCallUrlEncoded(strWSName: String,method:WSHttpMethod,accessToken:String,params : [String : Any],body : Data,isShowLoader:Bool, checksum:String, success:@escaping wsSuccessBlock, failure:@escaping wsFailureBlock) {
        
        
        if isShowLoader{
            //write code to show loader
            self.showNetworkActivity(show: true)
            
            SKActivityIndicator.spinnerStyle(.defaultSpinner)
            SKActivityIndicator.statusTextColor(Constants.AppColor.themeColor)
            SKActivityIndicator.show("Please wait...",userInteractionStatus: false)
            
        }
        
        //append WSName in base url
        var combineUrl = Constants.Networking.baseUrl + strWSName
        
        print("Fetching data for URL : \(combineUrl)")
        
        var urlRequest:URLRequest
        
        
        
        
        //if method is get append param in url
        if method == .get{
            combineUrl = combineUrl + params.queryString
            guard let url = NSURL(string:combineUrl) else {
                return
            }
            urlRequest = URLRequest(url: url as URL)
            urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue(accessToken, forHTTPHeaderField: "Authorization")
            urlRequest.cachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad

        }
            //else send param in http body
            
        else
        {
            guard let url = NSURL(string:combineUrl) else {
                return
            }
            
            urlRequest = URLRequest(url: url as URL)
            
            
            //setup request and response content type
            urlRequest.cachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad

            urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue(accessToken, forHTTPHeaderField: "Authorization")
            
            urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("ci_session=h0l2a1g7p5j90kh766gdd99u1de91p1t", forHTTPHeaderField: "Cookie")
            
//            do {
//                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
//            } catch let error {
//                print(error.localizedDescription)
//            }
            
           urlRequest.httpBody = body
        }
        
        urlRequest.httpMethod = method.rawValue
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            
            
            if isShowLoader{
                //write code to stop loader
                
                self.showNetworkActivity(show: false)
                SKActivityIndicator.dismiss()
                
            }
            
            guard (data != nil) || (error != nil) else {
                failure(WSError(msg: "No response received from server", code:0, type: .api))
                return
            }
            
            if (data?.isEmpty) == true{
                failure(WSError(msg: "No response received from server", code:0, type: .api))
                return
            }
            
            var valuePrint = [String:Any]()
            if method == WSHttpMethod.get {
                valuePrint["combineUrl"] = combineUrl
            }
            valuePrint["apiUrl"] = Constants.Networking.baseUrl + strWSName
            valuePrint["httpMethod"] = method
            valuePrint["param"] = params
            
            guard error == nil, let json = self.jsonSerializationWithData(data: data)
                else {
                    let code = (error! as NSError).code
                    var msg = ""
                    switch code{
                    case NSURLErrorTimedOut:
                        msg = ErrorMsg.msgRequestTimeOut
                    case NSURLErrorNotConnectedToInternet,NSURLErrorCannotConnectToHost:
                        msg = ErrorMsg.msgNoInternet
                    case NSURLErrorNetworkConnectionLost:
                        msg = ErrorMsg.msgConnectionLost
                    default:
                        msg = ErrorMsg.msgServerError
                    }
                    
                    valuePrint["error"] = WSError(msg: msg, code: code, type: .server)
                    valuePrint["response"] = nil
                    debugPrint(valuePrint)
                    
                    failure(WSError(msg: msg, code: code, type: .server))
                    return
            }
            
            valuePrint["response"] = json
            valuePrint["error"] = nil
            debugPrint(valuePrint)
            
            success(WSResult(data: data!, value: json))
            return
            
        }
        
        task.resume()
    }
    
    /// method for serialization
    ///
    /// - Parameter data: response data
    /// - Returns: key value pair
    func jsonSerializationWithData(data:Data?) -> AnyObject?{
        
        guard (data != nil) else {
            return nil
        }
        do
        {
            return try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? AnyObject

//            return try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
        }
        catch
        {
            print("error in JSONSerialization")
        }
        return nil
        
    }
    
    
    /// show network activity
    ///
    /// - Parameter show: true to show/false to hide
    private func showNetworkActivity(show:Bool){
        //show loader
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = show
        }
    }
    
}
