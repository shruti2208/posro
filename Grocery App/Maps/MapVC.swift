//
//  MapVC.swift
//  Maidan
//
//  Created by Shruti Gawas on 16/01/20.
//  Copyright © 2020 Shruti Gawas. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SkyFloatingLabelTextField
import SKActivityIndicatorView
import DropDown

class MapVC: UIViewController, UITextFieldDelegate {
    var userInfo : LoginModel!
    
    var selectedAdd : Add!
    let cityDropdown = DropDown()
    var lat : String!
    var lon : String!
    var cityId : String = ""
    var addType : String = ""
    
    var editMode : Bool = false
    //    var editObj : SavedAddressModelElement!
    var locUpdated : Bool = false
    private let locationManager1 = LocationManager()
    @IBOutlet weak var tfPhone: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfLandmark: SkyFloatingLabelTextField!
    @IBOutlet weak var tfAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var tfState: SkyFloatingLabelTextField!
    @IBOutlet weak var tfCity: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPincode: SkyFloatingLabelTextField!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnOffice: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    @IBOutlet weak var mapView: GMSMapView!
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 1000
    
    @IBOutlet weak var addressLabel: UILabel!
    
    let marker: GMSMarker = GMSMarker() // Allocating Marker
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tfPhone.delegate = self
        self.title = "Address"
//        let navBarAppearance = UINavigationBarAppearance()
//        navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//
//        navBarAppearance.configureWithTransparentBackground()
//        self.navigationController?.navigationBar.standardAppearance = navBarAppearance
//        self.navigationController?.navigationBar.compactAppearance = navBarAppearance
//        self.setBackgroundImage("bg1")
        
        
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "userInfo") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(LoginModel.self, from: savedPerson) {
                userInfo = loadedPerson
            }
        }
        
        if selectedAdd != nil {
            setValues()
        }
        tfCity.addTarget(self, action: #selector(didChangeText), for: .allTouchEvents)
        getCities()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        //        loadView()
        if (mapView != nil) {
            mapView.delegate = self
        }
        mapView.animate(toZoom: 15)
        mapView.tintColor = Constants.AppColor.themeColor
        
        setUpUI()
        //        if editMode == true{
        //            let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(self.editObj.lat!)!, Double(editObj.lon!)!)
        //
        //            self.mapView.camera = GMSCameraPosition(target: locValue, zoom: 15, bearing: 0, viewingAngle: 0)
        //
        //            self.locationManager.stopUpdatingLocation()
        //
        //            self.tfAddress.text = editObj.addressLine1
        //            self.tfState.text = editObj.state
        //            self.tfCity.text = editObj.city
        //            self.tfPincode.text = editObj.pincode
        //            self.tfAddName.text = editObj.addressName
        //            self.tfLandmark.text = editObj.landmark
        //        }
        
        marker.title = "Title" // Setting title
        marker.snippet = "Sub title" // Setting sub title
        marker.icon = UIImage(named: "") // Marker icon
        marker.appearAnimation = .pop // Appearing animation. default
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool
      {
          if textField == tfPhone{
          let currentCharacterCount = textField.text?.count ?? 0
          if (range.length + range.location > currentCharacterCount){
              return false
          }
          let newLength = currentCharacterCount + string.count - range.length
          return newLength <= 10
          }
          else{
              return true
          }
      }
    @objc func didChangeText(textField:UITextField) {
        let str = textField.text
        
        //your functionality here
        cityDropdown.show()
        
    }
    
    func setUpDropDown(arr : [Cities]) {
        // Not setting the anchor view makes the drop down centered on screen
        var tempArr : [String] = []
        
        for city in arr {
            tempArr.append(city.cityName)
        }
        cityDropdown.anchorView = tfCity // UIView or UIBarButtonItem
        
        cityDropdown.dataSource = tempArr
        cityDropdown.cellHeight = 40
        cityDropdown.separatorColor = .clear
        cityDropdown.backgroundColor = .white
        cityDropdown.selectionAction = { [weak self] (index, item) in
            self?.tfCity.text = item
            print("selected :", self!.cityDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            let selectedCity : Cities = arr[index]
            self!.cityId = selectedCity.cityID
            
        }
        
        
        
    }
    @IBAction func addAddress(){
        if addType == ""{
            self.alert(message: "Select Address Type")
        }
        else if cityId == ""{
            self.alert(message: "Select City")
        }
        else{
            
            let message: [String: Any] = [:]
            let parameters : String!
            let manager = WSManager.shared
            if selectedAdd != nil{
                parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&address_id=\(selectedAdd.id)&name=\(addType)&mobile=\(userInfo.data.contact)&address=\(tfAddress.text!)&pincode=\(tfPincode.text!)&city=\(tfCity.text!)&locality=\(tfState.text!)&landmark=\(tfLandmark.text!)&lat=\(lat!)&lon=\(lon!)&city_id=\(cityId)&address_type=\(addType)"
            }
            else{
                parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&address_id=0&name=\(addType)&mobile=\(userInfo.data.contact)&address=\(tfAddress.text!)&pincode=\(tfPincode.text!)&city=\(tfCity.text!)&locality=\(tfState.text!)&landmark=\(tfLandmark.text!)&lat=\(lat!)&lon=\(lon!)&city_id=\(cityId)&address_type=\(addType)"
            }
            let postData =  parameters.data(using: .utf8)
            
            
            manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.add_edit_address, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
                
                
                
                guard let model: AddressAddedModel = Utility.decodeData(data: result.data)
                    else {
                        let model1: VerifiedOtpModel = Utility.decode(data: result.data)!
                        DispatchQueue.main.async {
//                            self.alert(message:(model1.msg!))
                            if model1.code == 200{
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: SavedAddress.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                        }
                        return
                }
                print(model)
                DispatchQueue.main.async {
                    if model.status == true{
                        //                self.dismiss(animated: true, completion: nil)
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: SavedAddress.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
            }) { (error) in
                
                
                Utility.handleNetworkError(error: error)
                
            }
        }
    }
    func getCities(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_all_city_info, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: CitiesModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                self.setUpDropDown(arr: model.data)
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func setUpUI() {
        tfPhone.setBottomBorder(color: UIColor.lightGray)
        tfAddress.setBottomBorder(color: UIColor.lightGray)
        tfState.setBottomBorder(color: UIColor.lightGray)
        tfCity.setBottomBorder(color: UIColor.lightGray)
        tfPincode.setBottomBorder(color: UIColor.lightGray)
        tfLandmark.setBottomBorder(color: UIColor.lightGray)
        
        btnContinue.cornerRadius = 3
        
        tfPhone.titleFormatter = { $0 }
        tfAddress.titleFormatter = { $0 }
        tfState.titleFormatter = { $0 }
        tfCity.titleFormatter = { $0 }
        tfPincode.titleFormatter = { $0 }
        tfLandmark.setBottomBorder(color: UIColor.lightGray)
        
    }
    func fetchNearbyPlaces(exposedLocation : CLLocation){
        //        guard let exposedLocation = self.locationManager.exposedLocation else {
        //              print("*** Error in \(#function): exposedLocation is nil")
        //              return
        //          }
        
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "userInfo") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(LoginModel.self, from: savedPerson) {
                userInfo = loadedPerson
            }
        }
        tfPhone.text = userInfo.data.contact
        let locationManager1 = LocationManager()
        
        locationManager1.getPlace(for: exposedLocation) { placemark in
            guard let placemark = placemark else { return }
            
            //            var output = "Our location is:"
            if let Name = placemark.locality {
                //                output = output + "\n\(Name)"
                self.tfState.text = Name
            }
            if let postalCode = placemark.postalCode {
                //                output = output + "\n\(state)"
                self.tfPincode.text = postalCode
                
            }
            if let postalCode = placemark.thoroughfare {
                //                output = output + "\n\(state)"
                self.tfLandmark.text = postalCode
                
            }
            
        }
    }
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            //        self.addressLabel.unlock()
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            print(coordinate.latitude,coordinate.longitude)
            self.lat = String(coordinate.latitude)
            self.lon = String(coordinate.longitude)
            
            self.marker.position = coordinate // CLLocationCoordinate2D
            self.marker.map = self.mapView
            
            print(lines)
            if self.editMode == false{
            self.tfAddress.text = lines.joined(separator: "\n")
            }
            //            self.setCurrentLocation()
            let labelHeight = self.addressLabel.intrinsicContentSize.height
//            self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
//                                                bottom: labelHeight, right: 0)
            
            UIView.animate(withDuration: 0.25) {
                //          self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                self.view.layoutIfNeeded()
            }
            
        }
    }
    
}
// MARK: - CLLocationManagerDelegate
//1
extension MapVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        if selectedAdd == nil{
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        fetchNearbyPlaces(exposedLocation: location)
        }
    }
}
// MARK: - GMSMapViewDelegate
extension MapVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        if selectedAdd == nil{
        reverseGeocodeCoordinate(position.target)
//        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //      addressLabel.lock()
        
        if (gesture) {
            //        mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else {
            return nil
        }
        //        guard let inforView = UIView.loadFromNib(named: <#T##String#>)
        guard let infoView = UIView.loadFromNib(named: "MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        
        infoView.nameLabel.text = placeMarker.place.name
        if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
        } else {
            infoView.placePhoto.image = UIImage(named: "generic")
        }
        
        return infoView
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //      mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    
    
    @IBAction func selectAddType(_ sender: UIButton) {
        if sender.tag == 0{
            addType = "Home"
            btnHome.tintColor = Constants.AppColor.themeColor
            btnOffice.tintColor = .darkGray
            btnOthers.tintColor = .darkGray
            
            btnHome.setTitleColor(Constants.AppColor.themeColor, for: .normal)
            btnOffice.setTitleColor(.darkGray, for: .normal)
            btnOthers.setTitleColor(.darkGray, for: .normal)
            
        }
        else if sender.tag == 1{
            addType = "Office"
            
            btnOffice.tintColor = Constants.AppColor.themeColor
            btnHome.tintColor = .darkGray
            btnOthers.tintColor = .darkGray
            
            btnOffice.setTitleColor(Constants.AppColor.themeColor, for: .normal)
            btnHome.setTitleColor(.darkGray, for: .normal)
            btnOthers.setTitleColor(.darkGray, for: .normal)
            
        }
        else if sender.tag == 2{
            addType = "Other"
            
            btnOthers.tintColor = Constants.AppColor.themeColor
            btnOffice.tintColor = .darkGray
            btnHome.tintColor = .darkGray
            
            
            btnOthers.setTitleColor(Constants.AppColor.themeColor, for: .normal)
            btnOffice.setTitleColor(.darkGray, for: .normal)
            btnHome.setTitleColor(.darkGray, for: .normal)
        }
        print("addType : ",addType)
    }
    @IBAction func updateLoc(_ sender: Any) {
        updateLocation()
    }
    func updateLocation()
    {
        //
        //        let message : [String: Any] = [:]
        //     var parameters = "Address_Line_1=\(tfAddress.text!)&Landmark=\(tfLandmark.text!)&City=\(tfCity.text!)&State=\(tfState.text!)&Pincode=\(tfPincode.text!)&Address_Name=\(tfAddName.text!)&lat=\(lat!)&lon=\(lon!)"
        //        if editMode == true{
        //             parameters = "Address_Line_1=\(tfAddress.text!)&Landmark=\(tfLandmark.text!)&City=\(tfCity.text!)&State=\(tfState.text!)&Pincode=\(tfPincode.text!)&Address_Name=\(tfAddName.text!)&lat=\(lat!)&lon=\(lon!)&Customer_Address_ID=\(editObj.customerAddressID!)"
        //
        //        }
        //
        //              let postData =  parameters.data(using: .utf8)
        //        let manager = WSManager.shared
        //
        //        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.AddUpdateAddress, method: .post, accessToken: UserDefaults.standard.string(forKey: "accessToken")!, params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
        //
        //            guard let model: Bool = Utility.decodeData(data: result.data)
        //                else {
        //                    let model1: ErrorModel = Utility.decode(data: result.data)!
        //                    print(model1.msg!)
        //                    DispatchQueue.main.async {
        //                        self.alert(message:(model1.msg!))
        //
        //                    }
        //                    return
        //            }
        //            print(model)
        //
        //
        //            let encoder = JSONEncoder()
        //            if let encoded = try? encoder.encode(model) {
        //                let defaults = UserDefaults.standard
        //                defaults.set(encoded, forKey: "userInfo")
        //            }
        //
        //        }) { (error) in
        //
        //
        //            Utility.handleNetworkError(error: error)
        //
        //        }
        //
        //
        ////        var userInfo : LoginModel!
        ////
        ////        let defaults = UserDefaults.standard
        ////        if let savedPerson = defaults.object(forKey: "userInfo") as? Data {
        ////            let decoder = JSONDecoder()
        ////            if let loadedPerson = try? decoder.decode(LoginModel.self, from: savedPerson) {
        ////                userInfo = loadedPerson
        ////            }
        ////        }
        ////
        ////        SKActivityIndicator.spinnerStyle(.defaultSpinner)
        ////        SKActivityIndicator.statusTextColor(Constants.AppColor.themeColorTangerine)
        ////        SKActivityIndicator.show("Please wait...",userInteractionStatus: false)
        ////        let semaphore = DispatchSemaphore (value: 0)
        ////
        ////        let parameters = "Address_Line_1=\(tfAddress.text!)&Landmark=\(tfLandmark.text!)&City=\(tfCity.text!)&State=\(tfState.text!)&Pincode=\(tfPincode.text!)&Address_Name=\(tfAddName.text!)&lat=\(lat!)&lon=\(lon!)"
        ////        let postData =  parameters.data(using: .utf8)
        ////
        ////        var request = URLRequest(url: URL(string: APIEndpoint.AddUpdateAddress)!,timeoutInterval: Double.infinity)
        ////        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        ////        request.addValue(UserDefaults.standard.string(forKey: "accessToken")!, forHTTPHeaderField: "Authorization")
        ////
        ////
        ////
        ////        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        ////        request.addValue("ci_session=h0l2a1g7p5j90kh766gdd99u1de91p1t", forHTTPHeaderField: "Cookie")
        ////
        ////        request.httpMethod = "POST"
        ////        request.httpBody = postData
        ////
        ////        let task = URLSession.shared.dataTask(with: request) { data, response, error in
        ////            guard let data = data else {
        ////                print(String(describing: error))
        ////                return
        ////            }
        ////            print(String(data: data, encoding: .utf8)!)
        ////            SKActivityIndicator.dismiss()
        ////
        ////            if (String(data: data, encoding: .utf8)!) == "true"{
        ////
        ////                DispatchQueue.main.async {
        ////
        ////                    self.shouldPerformSegue(withIdentifier: "showGST", sender: self.btnContinue)
        ////                }
        ////            }
        ////            else{
        ////
        ////            }
        ////            semaphore.signal()
        ////        }
        ////
        ////        task.resume()
        ////        semaphore.wait()
    }
    func setValues(){
        
        if selectedAdd != nil {
            tfPhone.text = userInfo.data.contact
            tfState.text = selectedAdd.locality
            tfCity.text = selectedAdd.city
            tfPincode.text = selectedAdd.pincode
            tfLandmark.text = selectedAdd.landmark
            tfAddress.text = selectedAdd.address
            
            if selectedAdd.name == "Office"{
                addType = "Home"
                btnHome.tintColor = Constants.AppColor.themeColor
                btnOffice.tintColor = .darkGray
                btnOthers.tintColor = .darkGray
                
                btnHome.setTitleColor(Constants.AppColor.themeColor, for: .normal)
                btnOffice.setTitleColor(.darkGray, for: .normal)
                btnOthers.setTitleColor(.darkGray, for: .normal)
            }
            else if selectedAdd.name == "Home"{
                addType = "Office"
                
                btnOffice.tintColor = Constants.AppColor.themeColor
                btnHome.tintColor = .darkGray
                btnOthers.tintColor = .darkGray
                
                btnOffice.setTitleColor(Constants.AppColor.themeColor, for: .normal)
                btnHome.setTitleColor(.darkGray, for: .normal)
                btnOthers.setTitleColor(.darkGray, for: .normal)
            }
            else if selectedAdd.name == "Other"{
                addType = "Other"
                
                btnOthers.tintColor = Constants.AppColor.themeColor
                btnOffice.tintColor = .darkGray
                btnHome.tintColor = .darkGray
                
                
                btnOthers.setTitleColor(Constants.AppColor.themeColor, for: .normal)
                btnOffice.setTitleColor(.darkGray, for: .normal)
                btnHome.setTitleColor(.darkGray, for: .normal)
            }
            cityId = selectedAdd.cityID
            self.lat = String(selectedAdd.lat)
            self.lon = String(selectedAdd.lon)
            let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(self.selectedAdd.lat)!, Double(self.selectedAdd.lon)!)
            self.marker.position = locValue // CLLocationCoordinate2D
            self.marker.map = self.mapView
            self.mapView.camera = GMSCameraPosition(target: locValue, zoom: 15, bearing: 0, viewingAngle: 0)
            
            self.locationManager.stopUpdatingLocation()
            
        }
    }
}
