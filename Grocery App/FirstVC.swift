//
//  FirstVC.swift
//  Grocery App
//
//  Created by Shruti Gawas on 25/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnNewUser: UIButton!
    @IBOutlet var mainView: UIView!
    let loginVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"Login") as! Login
    let signUpVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"SignUp") as! SignUp


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setBackgroundImage("blurBg")
        
        btnNewUser.borderColor = Constants.AppColor.themeColorGreen
        btnNewUser.borderWidth = 1
        loginVC.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: mainView.frame.size.width, height:  mainView.frame.size.height)

        mainView.removeSubviews()
        mainView.addSubview(loginVC.view)
    }
    @IBAction func loginClicked(){
        mainView.removeSubviews()
        mainView.addSubview(loginVC.view)
        
        btnLogin.backgroundColor = Constants.AppColor.themeColorGreen
        btnNewUser.backgroundColor = .clear

        btnNewUser.borderWidth = 1
        btnNewUser.borderColor = Constants.AppColor.themeColorGreen

    }
    @IBAction func newUserClicked(){
        mainView.removeSubviews()
        mainView.addSubview(signUpVC.view)

        btnNewUser.backgroundColor = Constants.AppColor.themeColorGreen
        btnLogin.backgroundColor = .clear

             btnLogin.borderWidth = 1
             btnLogin.borderColor = Constants.AppColor.themeColorGreen
       }
}
