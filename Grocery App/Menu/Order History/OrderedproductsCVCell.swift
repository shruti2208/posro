//
//  OrderedproductsCVCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 17/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class OrderedproductsCVCell: UICollectionViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet var totalItems: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var vv: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        vv.layer.masksToBounds = false
//        vv.clipsToBounds = true
//
//        vv.layer.borderWidth = 0.5
//        vv.layer.borderColor = Constants.AppColor.themeColorGray.cgColor
    }
    
}
