//
//  Orders.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class Orders: UIViewController,UITableViewDelegate,UITableViewDataSource {
 
    var ordersModel : OrderHistoryModel!
    @IBOutlet var tbl: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Orders"
        tbl.register(UINib.init(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: "OrderHistoryCell")

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getOrderHistory()
    }
    func getOrderHistory(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"

        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_order_history, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: OrderHistoryModel = Utility.decodeData(data: result.data)
                else {
                    let model1: VerifiedOtpModel = Utility.decode(data: result.data)!
//                    print(model1.msg!)
//                    DispatchQueue.main.async {
//                        self.alert(message:(model1.msg!))
//
//                    }
                    return
            }
            print(model)
//            if model != nil {
            self.ordersModel = model
            DispatchQueue.main.async {
                
                self.tbl.reloadData()
            }
//            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ordersModel == nil{
            return 0
        }else{
       return ordersModel.data.count
    }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell =
         self.tbl.dequeueReusableCell(withIdentifier: "OrderHistoryCell",
                                      for: indexPath) as! OrderHistoryCell
        let order : OHData = ordersModel.data[indexPath.row]
        cell.orderNo.text = order.orderNo
        cell.contentView.backgroundColor = .clear
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-YYYY"

        if let date = dateFormatterGet.date(from: order.orderDate) {
            print(dateFormatterPrint.string(from: date))
            cell.date.text = dateFormatterPrint.string(from: date)

        } else {
           print("There was an error decoding the string")
        }
        
        cell.totalItem.text = order.products[0].totalitems
        cell.delStatus.text = order.orderStatus
        cell.total.text = "₹ " + order.orderTotal


        return cell
     }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 140
         
     }
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"OrderDetailed") as! OrderDetailed
        let order : OHData = ordersModel.data[indexPath.row]

        destinationVC.orderId = order.orderID
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.show(destinationVC, sender: self)
    }
}
