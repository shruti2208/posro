//
//  OrderDetailed.swift
//  Grocery App
//
//  Created by Shruti Gawas on 16/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class OrderDetailed: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet fileprivate var heightConstraint: NSLayoutConstraint!
    var fromCart : Bool = false

    var details : OrderDetailsModel!
    var orderId : String!
    @IBOutlet var date: UILabel!

    @IBOutlet var addresstype: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var landmark: UILabel!
    @IBOutlet var ph: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var btmView: UIView!
    @IBOutlet var btnCancel: UIButton!

    
    @IBOutlet var lblSubtotal: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDelCharges: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblGst: UILabel!
    
    @IBOutlet var lblPayment: UILabel!
    @IBOutlet var lblStatus: UILabel!
    
    @IBOutlet var cv: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        cv.register(UINib.init(nibName:"OrderedproductsCVCell", bundle: nil), forCellWithReuseIdentifier: "OrderedproductsCVCell")
        
        getDetails()
    }
    func setUpUI(){
        
        if details.data.orderStatus == "cancelled"{
            btnCancel.isEnabled = false
            btnCancel.alpha = 0.5
        }
        let dateFormatterGet = DateFormatter()
              dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

              let dateFormatterPrint = DateFormatter()
              dateFormatterPrint.dateFormat = "dd-MM-YYYY"

              if let date = dateFormatterGet.date(from: details.data.orderDate) {
                  print(dateFormatterPrint.string(from: date))
//                  cell.date.text = dateFormatterPrint.string(from: date)
                self.date.text = dateFormatterPrint.string(from: date)

              } else {
                 print("There was an error decoding the string")
              }
        addresstype.text = details.data.orderName
        address.text = details.data.orderAddress
        landmark.text = details.data.orderLandmark
        ph.text = details.data.orderMobile
        
        topView.borderColor = Constants.AppColor.themeColorTurquoise
        btmView.borderColor = Constants.AppColor.themeColorTurquoise
        
        topView.borderWidth = 0.5
        btmView.borderWidth = 0.5
        
        lblSubtotal.text = "₹ " + details.data.orderSubtotal
        lblDiscount.text = "₹ " + details.data.orderDiscount
        lblDelCharges.text = "₹ " + details.data.orderDelCharges
        lblTotal.text = "₹ " + String(details.data.orderSubtotal.floatValue + details.data.orderDelCharges.floatValue)
        lblGst.text = "₹ " + details.data.orderGst
        if details.data.orderPaymentType == "0"{
            lblPayment.text = "Cash On Delivery"

        }
        else{
            lblPayment.text = details.data.orderPaymentType

        }
        lblStatus.text = details.data.orderStatus
        
        
        
    }
    func getDetails(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&page_no=1&order_id=\(orderId!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_order_details, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: OrderDetailsModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.details = model
            DispatchQueue.main.async {
                
                //                self.tbl.reloadData()
                self.setUpUI()
                self.heightConstraint.constant = CGFloat(self.details.data.products.count * 80) + 100
                self.cv.layoutIfNeeded()
                
                self.cv.reloadData()
                self.view.layoutIfNeeded()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @IBAction func cancelOrder(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&order_id=\(details.data.orderID)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.order_cancellation, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: VerifiedOtpModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
//                    self.dismiss(animated: true, completion: nil)
                    self.showToast(message: "Order Cancelled")
                    for controller in self.navigationController!.viewControllers as Array {
                                         if controller.isKind(of: Orders.self) {
                                             self.navigationController!.popToViewController(controller, animated: true)
                                             break
                                         }
                                     }
                }
                else{
                    self.alert(message: model.data)
                    
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width  = (view.frame.width-20)
        return CGSize(width: self.view.frame.width - 40, height: 80)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if details == nil{
            return 0
        }
        else{
            print(details.data.products.count)
            return details.data.products.count
        }
    }
    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cV.dequeueReusableCell(withReuseIdentifier: "OrderedproductsCVCell", for: indexPath) as! OrderedproductsCVCell
        let obj : ODProduct = details.data.products[indexPath.row]
        cell.name.text = obj.name
        let price : Float = Float(obj.price)!
        let quantity : Float = Float(obj.quantity)!
        let totAL : Float = price * quantity
        cell.total.text = "Total : ₹" + String(totAL)
        cell.totalItems.text = obj.quantity
//        cell.img.downloaded(from:APIEndpoint.baseImg + "assets/uploads/" + obj.image)
        cell.img.loadImageUsingCache(withUrl: APIEndpoint.baseImg + "assets/uploads/" + obj.image)
        cell.img.contentMode = .scaleToFill
        return cell
    }

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if self.isMovingFromParent && fromCart == true{
        print("we are being popped")
//        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"Orders") as! Orders
////               let order : OHData = ordersModel.data[indexPath.row]
//
////               destinationVC.orderId = order.orderID
//               destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
//               self.show(destinationVC, sender: self)
        self.navigationController?.popToRootViewController(animated: true)

    }
}
}
