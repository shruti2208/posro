//
//  OrderHistoryCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 16/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    @IBOutlet var orderNo: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var totalItem: UILabel!
    @IBOutlet var delStatus: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet var vv: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        vv.addBottomShadowForView()
//        vv.layer.maskToBounds = false
        vv.layer.applySketchShadow(y: 0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension CALayer {
  func applySketchShadow(
    color: UIColor = .black,
    alpha: Float = 0.5,
    x: CGFloat = 0,
    y: CGFloat = 2,
    blur: CGFloat = 4,
    spread: CGFloat = 0)
  {
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
      shadowPath = nil
    } else {
      let dx = -spread
      let rect = bounds.insetBy(dx: dx, dy: dx)
      shadowPath = UIBezierPath(rect: rect).cgPath
    }
  }
}
