//
//  ContactUs.swift
//  Grocery App
//
//  Created by Shruti Gawas on 18/07/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {
    @IBOutlet var btnEmail: UIButton!
    @IBOutlet var btnPh: UIButton!
    var model : ContactUsModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Contact Us"
        getEmailPh()
    }
    @IBAction func emailPressed(){
        if let url = URL(string: "mailto:\(model.data.email)") {
                  if #available(iOS 10.0, *) {
                      UIApplication.shared.open(url)
                  } else {
                      UIApplication.shared.openURL(url)
                  }
    }
    }
    @IBAction func phPressed(){
           guard let number = URL(string: "tel://" + model.data.contact) else { return }
               UIApplication.shared.open(number)
       }
    func getEmailPh(){
            let message: [String: Any] = [:]
            
            let manager = WSManager.shared
            let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
            
            let postData =  parameters.data(using: .utf8)
            
            
            manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_contact_info, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
                
                
                
                guard let model: ContactUsModel = Utility.decodeData(data: result.data)
                    else {
                        let model1: UpdatedContactErrorModel = Utility.decode(data: result.data)!
                        print(model1.data)
    //                    DispatchQueue.main.async {
    //                        self.alert(message:(model1.msg!))
                            
    //                    }
                        return
                }
                print(model)
                self.model = model
                DispatchQueue.main.async {
                    self.btnEmail.setTitle(model.data.email, for: .normal)
                    self.btnPh.setTitle(model.data.contact, for: .normal)
                    
                    self.btnEmail.setImage(UIImage(named:"mail"), for: .normal)
                    self.btnPh.setImage(UIImage(named:"phone"), for: .normal)


                }
            }) { (error) in
                
                
                Utility.handleNetworkError(error: error)
                
            }
        }
}
