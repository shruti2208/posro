//
//  AboutUs.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import WebKit
import SKActivityIndicatorView

class AboutUs: UIViewController , WKNavigationDelegate {
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "About Us"
        SKActivityIndicator.spinnerStyle(.defaultSpinner)
        SKActivityIndicator.statusTextColor(Constants.AppColor.themeColor)
        
//                let url = URL(string: "https://www.hackingwithswift.com")!
        let url = URL(string: APIEndpoint.get_about)!
        print(url)
        
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SKActivityIndicator.dismiss()
        
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SKActivityIndicator.show("Please wait...",userInteractionStatus: false)
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SKActivityIndicator.dismiss()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
