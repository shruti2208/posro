//
//  ChangePhone.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/07/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ChangePhone: UIViewController {
    @IBOutlet var tfEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Change Mobile Number"
    }
    
   @IBAction func otp(){
        let message : [String: Any] = [:]
        let parameters = "API_KEY=FabCodersENCRYPT&mobile=\(tfEmail.text!)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&social=0"
        let postData =  parameters.data(using: .utf8)
        let manager = WSManager.shared
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.send_otp, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            guard let model: OtpModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"VerifyOTP") as! VerifyOTP
                    destinationVC.phone = self.tfEmail.text!
                    destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.show(destinationVC, sender: self)
                }
            }
            
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
