//
//  Profile.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
//import SCLAlertView
import SkyFloatingLabelTextField
class Profile: UIViewController {
    @IBOutlet var btnName: UIButton!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblPh: UILabel!
    @IBOutlet var btmView: UIView!

    var userName : String!
    var email : String!
    var ph : String!
    var userId : String!
    
    var editName : UITextField!
    var editEmail : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Profile"
        btmView.setBackgroundImage("btmView")
        
        setupUI()
    }
    func setupUI(){
        userName = UserDefaults.standard.string(forKey: "userName")
        email = UserDefaults.standard.string(forKey: "email")
        ph = UserDefaults.standard.string(forKey: "phone")
        userId = UserDefaults.standard.string(forKey: "userId")
        btnName.setTitle(userName, for: .normal)
        lblEmail.text = email
        lblPh.text = ph
    }
    @IBAction func editDetails(){
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false,
            dynamicAnimatorActive: false,
            buttonsLayout: .vertical
        )
        let alert = SCLAlertView(appearance: appearance)
        
        editName = alert.addTextField("Full Name")
        editName.text = userName
        //        editName.placeholder = "Full Name"
        editName.setBottomBorder(color: UIColor.lightGray)
        editName.borderWidth = 0
        editName.textAlignment = NSTextAlignment.left
        
        editEmail = alert.addTextField("Email")
        editEmail.text = email
        editEmail.borderWidth = 0
        editEmail.setBottomBorder(color: UIColor.lightGray)
        editEmail.textAlignment = NSTextAlignment.left
        
        _ = alert.addButton("SUBMIT", target:self, selector:#selector(Profile.submitClicked))
        _ = alert.addButton("CANCEL") {
            print("CANCEL button tapped")
        }
        
        let color = Constants.AppColor.themeColor
        
        _ = alert.showCustom("Edit Details", subTitle: "", color: color, circleIconImage: nil)
    }
    @objc func submitClicked(sender: UIButton){
        print(editName.text!,editEmail.text!)
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "user_id=\(userId!)&name=\(editName.text!)&email=\(editEmail.text!)&API_KEY=FabCodersENCRYPT"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_user_profile, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: UpdatedContactModel = Utility.decodeData(data: result.data)
                else {
                    let model1: UpdatedContactErrorModel = Utility.decode(data: result.data)!
                    print(model1.data)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.data))
                        
                    }
                    return
            }
            print(model)
            let defaults = UserDefaults.standard
            
            defaults.set(model.data.name, forKey: "userName")
            defaults.set(model.data.email, forKey: "email")
            defaults.set(model.data.contact, forKey: "phone")
            DispatchQueue.main.async {
                self.setupUI()
                
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @IBAction func logout(){
          let defaults = UserDefaults.standard
           defaults.set(false, forKey: "loggedIn")
           let vc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"FirstNav") as! FirstNav

           self.definesPresentationContext = true
           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
           let app : AppDelegate = UIApplication.shared.delegate as! AppDelegate
           app.window!.rootViewController = vc
      }
    @IBAction func changePhone(){
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ChangePhone") as! ChangePhone

               destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
               self.show(destinationVC, sender: self)
//        self.present(destinationVC, animated: true, completion: nil)
    }
}



