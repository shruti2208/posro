//
//  CartProductsCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 11/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import ValueStepper
class CartProductsCell: UICollectionViewCell {
    @IBOutlet var vv: UIView!
    @IBOutlet var selectionView: UIView!

    @IBOutlet var stepper: ValueStepper!

    @IBOutlet var btnDrop: UIButton!
    @IBOutlet var btnRemove: UIButton!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblRate: UILabel!
    @IBOutlet var lblTotal: UILabel!
    
    @IBOutlet var image: UIImageView!
    
    @IBOutlet var tfQty: UITextField!
    @IBOutlet var lblLocalName: UILabel!
    @IBOutlet var btnUpdate: UIButton!
    @IBOutlet var btnInfo: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        tfQty.setBottomBorder(color: UIColor.lightGray)
        btnRemove.cornerRadius = 10
//        btnUpdate.cornerRadius = 10
        
        vv.layer.borderWidth = 0.5
        vv.layer.borderColor = Constants.AppColor.themeColor.cgColor
    }
    
}
