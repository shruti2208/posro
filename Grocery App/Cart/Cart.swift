//
//  Cart.swift
//  Grocery App
//
//  Created by Shruti Gawas on 09/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import ValueStepper
import DropDown
class Cart: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIPopoverPresentationControllerDelegate {
    var grandTotal : Double!
    var couponDiscount : Float!
    var timeSlotId : String!
    var delDate : String = "Today"
    let unitDropdown = DropDown()
    
    let dayDropdown = DropDown()
    let timeDropdown = DropDown()
    var products : [Product1] = []
    var timeModel : [time] = []
    @IBOutlet fileprivate var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet var cv: UICollectionView!
    @IBOutlet var btnCheckout: UIButton!
    @IBOutlet var btnDay: UIButton!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var btnCheckCoupon: UIButton!
    
    @IBOutlet var lblSubtotal: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblDelCharges: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblGst: UILabel!
    
    @IBOutlet var tfCoupon: UITextField!
    
    var cartObj : CartModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        cv.register(UINib.init(nibName: "CartProductsCell", bundle: nil), forCellWithReuseIdentifier: "CartProductsCell")
        heightConstraint.constant = 0
        self.btnDay.setTitle("Today", for: .normal)
        self.btnTime.setTitle("Select Time", for: .normal)
        
        btnDay.borderColor = Constants.AppColor.themeColor
        btnDay.borderWidth = 1.0
        btnDay.setTitleColor(Constants.AppColor.themeColor, for: .normal)
        btnTime.borderColor = Constants.AppColor.themeColor
        btnTime.borderWidth = 1.0
        btnTime.setTitleColor(Constants.AppColor.themeColor, for: .normal)
        
        btnCheckout.layer.cornerRadius = 8
        btnCheckCoupon.layer.cornerRadius = 8
        setUpUnitDropDown()
        getCartDetails()
        getTimeSlot()
        //        if cartObj.data == nil{
        //        }
    }
    func setUpUI(){
      
        let subTotal = Double(cartObj.summary.subTotal!)
        let totalDiscount = Double(cartObj.summary.totalDiscount!)
        let deliveryCharges = Double(cartObj.summary.deliveryCharges!)
        let totalGst = Double(cartObj.summary.totalGst!)

        grandTotal = subTotal! + totalDiscount! + deliveryCharges! + totalGst!
        cartObj.summary.grandTotal = String(grandTotal)
        lblSubtotal.text = String(format: "%.2f",subTotal!)
        lblDiscount.text = String(format: "%.2f",totalDiscount!)
        lblDelCharges.text = String(format: "%.2f",deliveryCharges!)
        lblTotal.text = String(format: "%.2f",grandTotal)
        lblGst.text = String(format: "%.2f",totalGst!)
        
    }
    func showAlert(){
        let appearance = SCLAlertView.SCLAppearance(
                  kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
                  kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                  kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
                  showCloseButton: false,
                  dynamicAnimatorActive: false,
                  buttonsLayout: .horizontal
              )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Ok") {
            print("Second button tapped")
        }
        alertView.addButton("Shop More") {
            print("Second button tapped")
            for controller in self.navigationController!.viewControllers as Array {
                                     if controller.isKind(of: Homescreen.self) {
                                         self.navigationController!.popToViewController(controller, animated: true)
                                         break
                                     }
                                 }
            }

        let color = Constants.AppColor.themeColor
        
        _ = alertView.showCustom(cartObj.msg!, subTitle: "", color: color, circleIconImage: nil)

        }
    func getCartDetails(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "appVersion=\(Constants.AppIdentity.appVersion!)&API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&page_no=all"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_cart, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: CartModel = Utility.decodeData(data: result.data)
                else {
                    let model1: GenericErrorModel = Utility.decode(data: result.data)!
                    //                    print(model1.msg!)
                    //                    DispatchQueue.main.async {
                    //                        self.alert(message:(model1.msg!))
                    //
                    //                    }
                    DispatchQueue.main.async {

                    let imageName = "EmptyBasket.png"
                          let image = UIImage(named: imageName)
                          let imageView = UIImageView(image: image!)
                          
                          imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                          imageView.contentMode = .scaleAspectFill
                              
                        self.view.addSubview(imageView)
                          
                    }
                    return
            }
            print(model)
            self.cartObj = model
            self.products = (model.data?.products!)! as [Product1]
            DispatchQueue.main.async {
                
                self.setUpUI()
                self.heightConstraint.constant = CGFloat((self.cartObj.data?.products?.count)! * 144) + 20
                self.cv.layoutIfNeeded()
                
                self.cv.reloadData()
//                if model.code == 201{
//                    self.showAlert()
//                }
            }
            
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
  
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cartObj == nil{
            return 0
        }
        else{
            return products.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width  = (view.frame.width-20)
        return CGSize(width: self.view.frame.width, height: 144)
        
    }
    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cV.dequeueReusableCell(withReuseIdentifier: "CartProductsCell", for: indexPath) as! CartProductsCell
        let obj : Product1 = products[indexPath.row]
        cell.lblName.text = obj.name!
        cell.lblLocalName.text = " (" + obj.localName! + ")"
//        cell.image.downloaded(from:APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        cell.image.loadImageUsingCache(withUrl: APIEndpoint.baseImg + "assets/uploads/" + obj.image!)

        cell.image.contentMode = .scaleToFill
//        cell.btnUpdate.addTarget(self, action: #selector(self.update), for: .touchUpInside)
        //        cell.btnUpdate.tag = indexPath.row
        cell.stepper.tag = indexPath.row
        cell.stepper.stepValue = 1
        cell.stepper.addTarget(self, action: #selector(self.valueChanged1), for: .valueChanged)

        cell.btnDrop.tag = indexPath.row
        cell.btnDrop.addTarget(self, action: #selector(showDropDown(sender:)), for: .touchUpInside)
        if obj.quantityType == "piece"{
            cell.btnDrop.isHidden = true
//            cell.tfQty.isHidden = false
            cell.stepper.isHidden = false
            cell.stepper.maximumValue = obj.quantityAvailable?.doubleValue as! Double
//            cell.tfQty.text = obj.quantity!
            cell.stepper.value = obj.quantity!.doubleValue

            if obj.pieceUnit == "12"{
                cell.lblQty.text = "1" + " doz"
                
            }
            else{
//                cell.lblQty.text = obj.quantity! + " Qty"
                cell.lblQty.text = obj.pieceUnit! + " Qty"

            }
            
        }
        else{
            cell.btnDrop.isHidden = false
//            cell.tfQty.isHidden = true
            cell.stepper.isHidden = true

            let q = Float(obj.quantity!)
            
            let qty : Float = q! * 1000
            if q! >= 1 {
                cell.btnDrop.setTitle(String(q!) + " kg", for: .normal)
                
            }
            else{
                cell.btnDrop.setTitle(String(qty) + " gms", for: .normal)
                
            }
            let weightUnit = Double(obj.weightUnit!)
            
            if weightUnit! >= 1.0{
                let weightUnit = Double(obj.weightUnit!)
                
                cell.lblQty.text = String(format: "%.2f",weightUnit!) + " kg"
                
            }
            else{
                let weightUnit = Double(obj.weightUnit!)
                
                cell.lblQty.text = String(format: "%.2f",weightUnit!) + " gms"
                
                
            }
            
            
            
            cell.setNeedsLayout()
            
            
        }
        cell.lblRate.text = "Rate: ₹" + obj.price!
        cell.lblTotal.text = "Total : " + String(obj.price!.floatValue * obj.quantity!.floatValue)

        //        let price : Float = Float(obj.price!)!
        //        let qty : Float = Float(obj.quantity!)!
        
        //        let tot : Float = price * qty
        //        cell.lblTotal.text = "Total: ₹" + String(tot)
        cell.btnRemove.addTarget(self, action: #selector(self.removeItem), for: .touchUpInside)
        cell.btnRemove.tag = indexPath.row
        
//        cell.btnInfo.addTarget(self, action: #selector(self.showInfo), for: .touchUpInside)
//        cell.btnInfo.tag = indexPath.row
        //        cell.stepper.value = Double(obj.quantity)!
        //        cell.stepper.stepValue = 1
        //        cell.stepper.maximumValue = Double(obj.quantityAvailable)!
        
        
        
        return cell
        
    }
    @objc func valueChanged1(sender: ValueStepper) {
        // Use sender.value to do whatever you want
        print(sender.tag,sender.value)
                 var product : Product1 = products[sender.tag]
        
        if product.quantity?.intValue != Int(sender.value) {
        addToCartFromStepper(sender: sender)
        }
    }
     func addToCartFromStepper(sender: ValueStepper) {
        // Use sender.value to do whatever you want
        //        print(sender.tag,sender.value)
        let point: CGPoint = sender.convert(.zero, to: self.cv)
        if let indexPath = self.cv!.indexPathForItem(at: point) {
            let cell = self.cv!.cellForItem(at: indexPath) as! CartProductsCell
            print(cell.lblName.text!)
            var product : Product1 = products[sender.tag]
//            if product.quantityType == "piece"{
            product.quantity = String(cell.stepper.value)
                
//            }
//            else{
//                var qty : String!
//
//                let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
//                let qq : Float = Float(q)!
//                let qUnit : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[1])!
//                if qUnit == "gms"{
//                    let qqq : Float = Float(qq / 1000)
//                    qty = String(qqq)
//                }
//                else{
//                    let qqq : Float = Float(qq)
//                    qty = String(qqq)
//                }
//
//
//                product.quantity = qty
//
//            }
            
            products[sender.tag] = product
            
            addToCart()
            
        }
        
        
    }
    @objc func showInfo(sender: UIButton) {
//        let vc = ProductInfo()
//        vc.modalPresentationStyle = .overCurrentContext
//        vc.modalTransitionStyle = .crossDissolve
//        present(vc, animated: true, completion: nil)
        
       let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductInfo") as! ProductInfo
        vc.product = products[sender.tag]
        vc.modalPresentationStyle = .popover
        let popover = vc.popoverPresentationController!
        popover.delegate = self
        popover.permittedArrowDirections = .right
        popover.sourceView = sender
        popover.sourceRect = sender.bounds
        vc.preferredContentSize = CGSize(width: 200, height: 280)
        vc.view.backgroundColor = UIColor.init(white: 0.7, alpha: 0.8)
        present(vc, animated: true, completion:nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    @objc func showDropDown(sender: UIButton){
        unitDropdown.anchorView = sender // UIView or UIBarButtonItem
        unitDropdown.show()
        unitDropdown.selectionAction = { [weak self] (index, item) in
            //               self?.btnDrop.setTitle(item, for: .normal)
            print("selected :", self!.unitDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            //            var item : Product = self!.offers.data.products[index]
            //                    let indexPath = IndexPath(row: index, section: 0)
            //            let cell: ProductCell = self!.cvCategory.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
            let point: CGPoint = sender.convert(.zero, to: self?.cv)
            if let indexPath = self?.cv!.indexPathForItem(at: point) {
                let cell = self?.cv!.cellForItem(at: indexPath) as! CartProductsCell
                print(cell.lblName.text!)
                cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
                
            }
            
            //                    let cell : ProductCell = self?.cvCategory.cellForItem(at: indexPath)
            //                    cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
            //                    self?.cvCategory.reloadData()
            
            //               self?.btnDrop.setTitleColor(UIColor.black, for: .normal)
        }
    }
    func removeFromCart(productId: String){
        
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&product_id=\(productId)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.remove_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                else {
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: result.data) as? [String: Any] {
                            DispatchQueue.main.async {
                                self.getCartDetails()
                            }
                            print(jsonResult)
                        }
                    } catch let parseError {
                        print("JSON Error \(parseError.localizedDescription)")
                    }
                    
                    
                    //                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    //                    print(model1.msg!)
                    //                    DispatchQueue.main.async {
                    //                        self.alert(message:(model1.msg!))
                    //
                    //                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                self.getCartDetails()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
        
    }
    @objc func removeItem(sender: UIButton) {
        //        products.remove(at: sender.tag)
        //        addToCart()
        removeFromCart(productId: products[sender.tag].id!)
    }
    @objc func update(sender: UIButton) {
        // Use sender.value to do whatever you want
        //        print(sender.tag,sender.value)
        let point: CGPoint = sender.convert(.zero, to: self.cv)
        if let indexPath = self.cv!.indexPathForItem(at: point) {
            let cell = self.cv!.cellForItem(at: indexPath) as! CartProductsCell
            print(cell.lblName.text!)
            var product : Product1 = products[sender.tag]
            if product.quantityType == "piece"{
                product.quantity = cell.tfQty.text
                
            }
            else{
                var qty : String!
                
                let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
                let qq : Float = Float(q)!
                let qUnit : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[1])!
                if qUnit == "gms"{
                    let qqq : Float = Float(qq / 1000)
                    qty = String(qqq)
                }
                else{
                    let qqq : Float = Float(qq)
                    qty = String(qqq)
                }
                
                
                product.quantity = qty
                
            }
            
            products[sender.tag] = product
            
            addToCart()
            
        }
        
        
    }
    @objc func buttonTapped(sender : UIButton) {
        //Write button action here
        print(sender.tag)
    }
    @IBAction func showDayDropDown(sender: UIButton){
        dayDropdown.anchorView = sender // UIView or UIBarButtonItem
        
        dayDropdown.show()
        //        self.setUpDropDown(arr: timeModel)
    }
    @IBAction func showTimeDropDown(sender: UIButton){
        timeDropdown.anchorView = sender // UIView or UIBarButtonItem
        
        timeDropdown.show()
    }
    func setUpDropDown(arr : [time]) {
        // Not setting the anchor view makes the drop down centered on screen
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        
        var tempArr : [String] = ["Today","Tomorrow"]
        var tempArr1 : [String] = []
        
        var dayIsToday : Bool = true
        
        dayDropdown.dataSource = tempArr
        dayDropdown.cellHeight = 40
        dayDropdown.separatorColor = .clear
        dayDropdown.backgroundColor = .white
        dayDropdown.selectionAction = { [weak self] (index, item) in
            self?.btnDay.setTitle(item, for: .normal)
            print("selected :", self!.dayDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            self?.delDate = self!.dayDropdown.selectedItem!
            if self!.dayDropdown.selectedItem! == "Tomorrow"{
                dayIsToday = false
                tempArr1.removeAll()
                for item in arr {
                    tempArr1.append(item.name + "(" + item.timeFrom + " - " + item.timeTo + ")")
                }
                self!.timeDropdown.dataSource = tempArr1
                
            }
            else{
                tempArr1.removeAll()
                
                dayIsToday = true
                for item in arr {
                    let ho = item.timeFrom.components(separatedBy: ":")
                    let hr = Int(ho.first!)
                    if hr! > hour{
                        tempArr1.append(item.name + "(" + item.timeFrom + " - " + item.timeTo + ")")
                    }
                }
                self!.timeDropdown.dataSource = tempArr1
                
            }
            
            
            
        }
        if dayIsToday == true{
            
            for item in arr {
                let ho = item.timeFrom.components(separatedBy: ":")
                let hr = Int(ho.first!)
                if hr! > hour{
                    tempArr1.append(item.name + "(" + item.timeFrom + " - " + item.timeTo + ")")
                }
            }
        }
        else{
            for item in arr {
                tempArr1.append(item.name + "(" + item.timeFrom + " - " + item.timeTo + ")")
            }
        }
        self.timeDropdown.dataSource = tempArr1
        self.timeDropdown.cellHeight = 40
        self.timeDropdown.separatorColor = .clear
        self.timeDropdown.backgroundColor = .white
        
        self.timeDropdown.selectionAction = { [weak self] (index, item) in
            self?.btnTime.setTitle(item, for: .normal)
            print("selected :", self!.timeDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            
            self!.timeSlotId = arr[index].id
            
        }
        
    }
    func setUpUnitDropDown() {
        // Not setting the anchor view makes the drop down centered on screen
        let tempArr : [String] = ["100 gms","250 gms","500 gms","1.0 kg","2 kg","3 kg","5 kg","10 kg"]
        
        unitDropdown.dataSource = tempArr
        unitDropdown.cellHeight = 40
        unitDropdown.separatorColor = .clear
        unitDropdown.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //        unitDropdown.selectionAction = { [weak self] (index, item) in
        //            //               self?.btnDrop.setTitle(item, for: .normal)
        //            print("selected :", self!.unitDropdown.selectedItem as Any)
        //            print("selectedIndex :", index)
        //            //            var item : Product = self!.offers.data.products[index]
        //            let indexPath = IndexPath(row: index, section: 0)
        ////            let cell: ProductCell = self!.cvCategory.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        //
        //            let cell : ProductCell = self?.cvCategory.cellForItem(at: indexPath)
        //            print(cell.lblName.text!)
        //            cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
        //            self?.cvCategory.reloadData()
        //
        //            //               self?.btnDrop.setTitleColor(UIColor.black, for: .normal)
        //        }
    }
    @IBAction func applyCoupon(sender: UIButton){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&coupon_code=\(tfCoupon.text!)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_coupon_validity, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: CouponValidityModel = Utility.decodeData(data: result.data)
                else {
                    let model1: InvalidCoupon = Utility.decode(data: result.data)!
                    print(model1.data.message)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.data.message))
                        
                    }
                    return
            }
            print(model)
            
            DispatchQueue.main.async {
                if model.status == true{
                    self.couponDiscount = model.data.discount?.floatValue
                    self.reCalc(discount:self.couponDiscount)
                }
//                self.alert(message:(model.data.message))
                
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func reCalc(discount : Float){
        let subTotal = (cartObj.summary.subTotal!.floatValue * discount)/100
        let gst = (cartObj.summary.totalGst!.floatValue * discount)/100
        let grand = (cartObj.summary.grandTotal!.floatValue * discount)/100

        cartObj.summary.subTotal = String(subTotal)
        cartObj.summary.totalGst = String(gst)
        cartObj.summary.grandTotal = String(grand)

        self.setUpUI()

    }
    @IBAction func checkout(sender: UIButton){
        let subgst = cartObj.summary.totalGst!.floatValue + cartObj.summary.subTotal!.floatValue
        
        if subgst < cartObj.summary.minOrderAmt!.floatValue{
            showAlert()
        }
        else if self.timeSlotId == nil{
            self.showToast(message: "Select Time slot")
        }
        else{
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.check_stock_available, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: VerifyStockModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    //                    self.alert(message:(model.msg))
                    let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"SavedAddress") as! SavedAddress
                    destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    destinationVC.cartObj = self.cartObj
                    destinationVC.timeSlotId = self.timeSlotId
                    destinationVC.delDate = self.delDate
                    destinationVC.fromCart = true

                    self.show(destinationVC, sender: self)
                }
                else{
                    self.alert(message:(model.msg))
                    
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
        }
    }
    func getTimeSlot(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_timeslot, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: TimeslotModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.timeModel = model.data
            DispatchQueue.main.async {
                self.setUpDropDown(arr: model.data)
                
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func addToCart(){
        var mainArr : [NSMutableDictionary] = []
        
        for obj in products {
            //            let obj : Product1 = products[sender.tag]
            
            var dict : NSMutableDictionary = [:]
            dict.setValue(obj.categoryID, forKey: "categoryId")
            dict.setValue(obj.cgst, forKey: "cgst")
            dict.setValue(obj.discount, forKey: "discount")
            dict.setValue(obj.id, forKey: "id")
            dict.setValue(obj.categoryID, forKey: "categoryimageUrlId")
            dict.setValue(obj.image, forKey: "imageUrl")
            dict.setValue(false, forKey: "loading")
            dict.setValue(obj.localName, forKey: "localName")
            dict.setValue(obj.name, forKey: "name")
            dict.setValue(obj.price, forKey: "price")
            dict.setValue(obj.quantityAvailable, forKey: "qtyAvailable")
            dict.setValue(obj.quantityType, forKey: "qtyType")
            dict.setValue(obj.sgst, forKey: "sgst")
            dict.setValue(obj.status, forKey: "status")
            dict.setValue(obj.price, forKey: "total")
            dict.setValue(obj.pieceUnit, forKey: "unitPiece")
            dict.setValue(obj.weightUnit, forKey: "unitWeight")
            dict.setValue(obj.quantity, forKey: "qtyAdded")
            
            mainArr.append(dict)
            print(dict)
        }
        let str = Utility.convertIntoJSONString(arrayObject: mainArr)
        
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&products=\(str!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                else {
                    do {
                        if let jsonResult = try JSONSerialization.jsonObject(with: result.data) as? [String: Any] {
                            DispatchQueue.main.async {
                                self.getCartDetails()
                            }
                            print(jsonResult)
                        }
                    } catch let parseError {
                        print("JSON Error \(parseError.localizedDescription)")
                    }
                    
                    
                    //                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    //                    print(model1.msg!)
                    //                    DispatchQueue.main.async {
                    //                        self.alert(message:(model1.msg!))
                    //
                    //                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                self.getCartDetails()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
}
