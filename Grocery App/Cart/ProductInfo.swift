//
//  ProductInfo.swift
//  Grocery App
//
//  Created by Shruti Gawas on 02/07/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ProductInfo: UIViewController {
    @IBOutlet var lblBasicRate: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblSgst: UILabel!
    @IBOutlet var lblCgst: UILabel!
    @IBOutlet var lblFinal: UILabel!

    var product : Product1!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if product != nil{
            lblBasicRate.text = "₹" + product.price!
            lblDiscount.text = "₹" + product.discount!
        lblQty.text = product.quantity
        let qty : Float = (product.quantity! as NSString).floatValue
        let price :  Float = (product.price! as NSString).floatValue
        let total : Float = qty * price
        lblTotal.text = "Total ₹" + String(total)
            lblSgst.text = "SGST ₹" + product.sgst!
            lblCgst.text = "CGST ₹" + product.cgst!
        let final : Float = total + (product.sgst! as NSString).floatValue + (product.cgst! as NSString).floatValue
        lblFinal.text = "Final Total ₹" + String(final)
        }
    }

}
