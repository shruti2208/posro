//
//  SavedAddressCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 12/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import CheckMarkView

class SavedAddressCell: UITableViewCell {
    @IBOutlet weak var checkMarkView: CheckMarkView!

    @IBOutlet var addType: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var landmark: UILabel!
    @IBOutlet var ph: UILabel!
    @IBOutlet var vv: UIView!

    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var img: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        vv.addBottomShadowForView()
        vv.layer.applySketchShadow(y: 0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        let padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
//        bounds = bounds.inset(by: padding)
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0))

    }
    
}
