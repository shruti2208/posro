//
//  SavedAddress.swift
//  Grocery App
//
//  Created by Shruti Gawas on 12/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class SavedAddress: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var cartObj : CartModel!
    var timeSlotId : String!
    var delDate : String!
    var fromCart : Bool = false
    @IBOutlet var btnProceed: UIButton!
    
    @IBOutlet var tbl: UITableView!
    var addresses : [Add] = []
    var selectedIndices : [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tbl.register(UINib.init(nibName: "SavedAddressCell", bundle: nil), forCellReuseIdentifier: "SavedAddressCell")
        tbl.allowsMultipleSelection = false
        if fromCart == false{
            btnProceed.isHidden = true
            tbl.allowsSelection = false
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getSavedAdd()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        addresses.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =
            self.tbl.dequeueReusableCell(withIdentifier: "SavedAddressCell",
                                         for: indexPath) as! SavedAddressCell
        //                       cell.checkMarkView.checked = !cell.checkMarkView.checked
        
        let tempArr = selectedIndices
        if tempArr.contains(indexPath.row){
            var idxToRemove : Int!
            for item in 0...selectedIndices.count{
                if selectedIndices[item] == indexPath.row{
                    idxToRemove = item
                    break
                }
            }
            selectedIndices.remove(at: idxToRemove)
        }
        else{
            selectedIndices.removeAll()
            selectedIndices.append(indexPath.row)
        }
        tbl.reloadData()
        
        //        let currentCell = self.tbl.cellForRow(at: indexPath) as! SavedAddressCell
        //        currentCell.vv.layer.borderColor = Constants.AppColor.themeColor.cgColor
        //        currentCell.vv.layer.borderWidth = 1.0
        
        //        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        let cell = tbl.cellForRow(at: indexPath as IndexPath) as! SavedAddressCell
        //           }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            self.tbl.dequeueReusableCell(withIdentifier: "SavedAddressCell",
                                         for: indexPath) as! SavedAddressCell
        //        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.checkMarkView.style = .nothing
        cell.checkMarkView.setNeedsDisplay()
        cell.checkMarkView.shadow = false
        
        cell.addType.text = addresses[indexPath.row].name
        cell.address.text = addresses[indexPath.row].address
        cell.landmark.text = addresses[indexPath.row].landmark
        cell.ph.text = addresses[indexPath.row].mobile
        cell.checkMarkView.checked = false
        
        if selectedIndices.contains(indexPath.row){
            //            cell.vv.layer.borderColor = Constants.AppColor.themeColor.cgColor
            //            cell.vv.layer.borderWidth = 0.5
            //            cell.img.image = UIImage(named: "check")
            cell.checkMarkView.checked = true
        }
        else{
            //            cell.vv.layer.borderWidth = 0
            //            cell.img.image = UIImage(named: "")
            cell.checkMarkView.checked = false
            
        }
        cell.btnDelete.addTarget(self, action: #selector(self.removeItem), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(self.editItem), for: .touchUpInside)
        cell.btnEdit.tag = indexPath.row
        
        
        return cell
        
    }
    func getSavedAdd(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_user_addresses, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: SavedAddressesModel = Utility.decodeData(data: result.data)
                else {
                    let model1: UpdatedContactErrorModel = Utility.decode(data: result.data)!
                    print(model1.data)
                    //                    DispatchQueue.main.async {
                    //                        self.alert(message:(model1.msg!))
                    
                    //                    }
                    return
            }
            print(model)
            self.addresses = model.data
            DispatchQueue.main.async {
                self.tbl.reloadData()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @objc func editItem(sender: UIButton) {
        let obj : Add!
        obj = addresses[sender.tag]
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"MapVC") as! MapVC
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        destinationVC.selectedAdd = obj
        destinationVC.editMode = true
        self.show(destinationVC, sender: self)
    }
    @objc func removeItem(sender: UIButton) {
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&address_id=\(addresses[sender.tag].id)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.delete_address, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: DelAdd = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    self.alert(message: "Address deleted Successfully")
                }
                self.getSavedAdd()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @IBAction func confirm(){
        
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"OrderDetails") as! OrderDetails
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        destinationVC.cartObj = self.cartObj
        destinationVC.addressObj = self.addresses[selectedIndices[0]]
        destinationVC.timeSlotId = self.timeSlotId
        destinationVC.delDate = self.delDate
        self.show(destinationVC, sender: self)
        
    }
}
