//
//  OrderPlaced.swift
//  Grocery App
//
//  Created by Shruti Gawas on 13/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class OrderPlaced: UIViewController {
    var orderId : String!
    @IBOutlet var lblOrderId: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lblOrderId.text = "Thank you for your order Your order ID is: " + orderId
    }
    
    @IBAction func okClicked(){
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"NavController") as! NavController
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.show(destinationVC, sender: self)
    }
    @IBAction func showDetails(){
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"OrderDetailed") as! OrderDetailed
        //               let order : OHData = ordersModel.data[indexPath.row]
        
        destinationVC.orderId = orderId
        destinationVC.fromCart = true
        
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.show(destinationVC, sender: self)
    }
}
