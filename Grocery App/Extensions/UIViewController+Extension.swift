//
//  UIViewController+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Add Left button on navigation bar (say back button)
    ///
    /// - Parameters:
    ///   - btnImg: pass image name, set nil or blank if not needed
    ///   - text: pass title name, set nil or blank if not needed
    func addLeftButton(btnImg:String?,text:String?) {
        
        let button = UIButton(type: .custom)
        if btnImg != "" && btnImg != nil{
            button.setImage(UIImage(named: btnImg!), for: .normal)
        }
        if text != "" && text != nil{
            button.setTitle(text, for: .normal)
//            button.titleLabel?.font = Constants.AppFont.navigationButton
        }
        button.sizeToFit()
        
        button.addTarget(self, action: #selector(actionLeftNavButton), for: .touchUpInside)
        
        let leftBarItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItem = leftBarItem
    }
    
    /// add right bar button on navigation bar
    ///
    /// - Parameters:
    ///   - btnImg1: image name for button 1, pass blank or nil if not needed
    ///   - btnImg2: image name for button 2, pass blank or nil if not needed
    ///   - text1: title for button 1, pass blank or nil if not needed
    ///   - text2: title for button 2, pass blank or nil if not needed
    func addRightBtn(btnImg1:String?,btnImg2:String?,text1:String?,text2:String?) {
        
        var arrBarButtonItem = [UIBarButtonItem]()
        if (btnImg1 != "" && btnImg1 != nil) || (text1 != "" && text1 != nil){
            let button = UIButton(type: .custom)
            if btnImg1 != "" && btnImg1 != nil{
                button.setImage(UIImage(named: btnImg1!), for: .normal)
            }
            if text1 != "" && text1 != nil{
                button.setTitle(text1, for: .normal)
//                button.titleLabel?.font = Constants.AppFont.navigationButton
            }
            button.sizeToFit()
            
            button.addTarget(self, action: #selector(actionRight1NavButton), for: .touchUpInside)
            
            let right1 = UIBarButtonItem(customView: button)
            arrBarButtonItem.append(right1)
        }
        
        if (btnImg2 != "" && btnImg2 != nil) || (text2 != "" && text2 != nil){
            let button = UIButton(type: .custom)
            if btnImg2 != "" && btnImg2 != nil{
                button.setImage(UIImage(named: btnImg2!), for: .normal)
            }
            if text2 != "" && text2 != nil{
                button.setTitle(text2, for: .normal)
//                button.titleLabel?.font = Constants.AppFont.navigationButton
                
            }
            button.sizeToFit()
            button.addTarget(self, action: #selector(actionRight2NavButton), for: .touchUpInside)
            let right2 = UIBarButtonItem(customView: button)
            arrBarButtonItem.append(right2)
        }
        
        self.navigationItem.rightBarButtonItems = arrBarButtonItem
        
    }
    
    /// Add button on navigation title
    ///
    /// - Parameter title: title of button
    func addTitleViewButton(title: String) {
        
        let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        button.backgroundColor = UIColor.clear
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = Constants.AppFont.navigationTitle
        button.addTarget(self, action: #selector(actionTitleViewButton), for: .touchUpInside)
        self.navigationItem.titleView = button
    }
    
    /// Helper method to add a UIViewController as a childViewController.
    ///
    /// - Parameters:
    ///   - child: the view controller to add as a child
    ///   - containerView: the containerView for the child viewcontroller's root view.
    func addChildViewController(_ child: UIViewController, toContainerView containerView: UIView,alpha:Float) {
        addChild(child)
        child.view.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(alpha))
        containerView.addSubview(child.view)
        child.didMove(toParent: self)
    }

    
    /// Helper method to remove a UIViewController from its parent.
    func removeViewAndControllerFromParentViewController() {
        guard parent != nil else { return }
        
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
    
    /// Get previous view controller from navigation stack
    ///
    /// - Returns: previous Viewcontroller
    func previousViewController() -> UIViewController? {
        if let stack = self.navigationController?.viewControllers {
            for i in (1..<stack.count).reversed() {
                if(stack[i] == self) {
                    return stack[i-1]
                }
            }
        }
        return nil
    }
    
    /// Adds image named: as a UIImageView in the Background
    func setBackgroundImage(_ named: String) {
        let image = UIImage(named: named)
        let imageView = UIImageView(frame: view.frame)
        imageView.image = image
        view.addSubview(imageView)
        view.sendSubviewToBack(imageView)
    }
    
    /// Adds UIImage as a UIImageView in the Background
    func setBackgroundImage(_ image: UIImage) {
        let imageView = UIImageView(frame: view.frame)
        imageView.image = image
        view.addSubview(imageView)
        view.sendSubviewToBack(imageView)
    }

    //MARK: action methods
    @objc func actionLeftNavButton(){
        
    }
    
    @objc func actionTitleViewButton() {
        
    }
    
    @objc func actionRight1NavButton(){
        
    }
    
    @objc func actionRight2NavButton(){
        
    }

}


extension UIViewController {
    
    func alert(message:String){
        alert(withTitle: "Alert!", message: message)
    }
    
    func alert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithRestart(message : String) {
        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
            alertController.dismiss(animated: true, completion: {
                UIViewController.restartApplication()
            })
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    static func restartApplication(){
        let vc = UIStoryboard(name:"LaunchScreen",bundle:nil).instantiateViewController(withIdentifier:"launcher")
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
}
extension UIViewController {

func showToast(message : String) {
    var height : CGFloat = 45
    if message.count > 100{
        height = 100
    }
    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 200, height: height))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = .systemFont(ofSize: 14.0)
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.numberOfLines = 0
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }
