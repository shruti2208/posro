//
//  UICollectionView+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit
import Foundation

extension UICollectionView {
    
    /// register collectionviewcell, make sure cell class name and identifier name is same
    ///
    /// - Parameters:
    ///   - cellType: Cell subclass
    ///   - bundle: bundle
    func register<T: UICollectionViewCell>(cellType: T.Type, bundle: Bundle = Bundle(for: T.self)) {
        let className = String(describing: cellType)
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: className)
    }
    
    /// register multiple collectionviewcell,, make sure cell class name and identifier name is same
    ///
    /// - Parameters:
    ///   - cellType: array of cell subclass
    ///   - bundle: bundle
    func register<T: UICollectionViewCell>(cellTypes: [T.Type], bundle: Bundle = Bundle(for: T.self)) {
        cellTypes.forEach { register(cellType: $0, bundle: bundle) }
    }
    
    /// register colllectionviewcell with nib,
    ///
    /// - Parameters:
    ///   - reusableViewType: ViewType
    ///   - kind: Element Kind
    ///   - bundle: bundle
    func register<T: UICollectionReusableView>(reusableViewType: T.Type,
                                               ofKind kind: String = UICollectionView.elementKindSectionHeader,
                                                      bundle: Bundle = Bundle(for: T.self)) {
        let className = String(describing: reusableViewType)
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: className)
    }
    
    /// register colllectionviewcells with nib
    ///
    /// - Parameters:
    ///   - reusableViewTypes: array of ViewType
    ///   - kind: Element Kind
    ///   - bundle: bundle
    func register<T: UICollectionReusableView>(reusableViewTypes: [T.Type],
                                               ofKind kind: String = UICollectionView.elementKindSectionHeader,
                                                      bundle: Bundle = Bundle(for: T.self)) {
        reusableViewTypes.forEach { register(reusableViewType: $0, ofKind: kind, bundle: bundle) }
    }
    
    /// get reusable cell
    ///
    /// - Parameters:
    ///   - type: cell type
    ///   - indexPath: indexpath
    /// - Returns: reusable cell
    func dequeueReusableCell<T: UICollectionViewCell>(with type: T.Type,
                                                             for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: type), for: indexPath) as! T
    }
    
    /// get reusableview
    ///
    /// - Parameters:
    ///   - type: view type
    ///   - indexPath: indexpath
    ///   - kind: kind
    /// - Returns: reusableview
    func dequeueReusableView<T: UICollectionReusableView>(with type: T.Type,
                                                                 for indexPath: IndexPath,
                                                                 ofKind kind: String = UICollectionView.elementKindSectionHeader) -> T {
        return dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: type), for: indexPath) as! T
    }
}
