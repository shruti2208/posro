//
//  Encodable+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 22/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation

extension Encodable {
    func encode() throws -> Data {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        return try encoder.encode(self)
    }
}
