//
//  String+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation
import UIKit

public extension String {
    
    /// encode emoji
     var encodeEmoji: String? {
        let encodedStr = NSString(cString: self.cString(using: String.Encoding.nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr as String?
    }
    
    /// decode emoji
     var decodeEmoji: String? {
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if decodedStr != nil{
            return decodedStr as String?
        }
        return self
    }
    
    /// get localized string
     var localized: String {
        return NSLocalizedString(self, comment: self)
    }
    
    /// get url from string
     var url: URL? {
        return URL(string: self)
    }
    
    /// Checks if string is empty or consists only of whitespace and newline characters
     var isBlank: Bool {
        let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    
    /// Trims white space and new line characters
     mutating func trim() {
        self = self.trimmed()
    }
    
    /// Checks if String contains Email
     var isEmail: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: count))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    /// Extracts URLS from String
     var extractURLs: [URL] {
        var urls: [URL] = []
        let detector: NSDataDetector?
        do {
            detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        } catch _ as NSError {
            detector = nil
        }
        
        let text = self
        
        if let detector = detector {
            detector.enumerateMatches(in: text,
                                      options: [],
                                      range: NSRange(location: 0, length: text.count),
                                      using: { (result: NSTextCheckingResult?, _, _) -> Void in
                                        if let result = result, let url = result.url {
                                            urls.append(url)
                                        }
            })
        }
        
        return urls
    }
        var floatValue: Float {
            return (self as NSString).floatValue
        }
    var intValue: Int {
              return (self as NSString).integerValue
          }
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
    
    /// Returns height of rendered string
     func height(_ width: CGFloat, font: UIFont, lineBreakMode: NSLineBreakMode?) -> CGFloat {
        var attrib: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font]
        if lineBreakMode != nil {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineBreakMode = lineBreakMode!
            attrib.updateValue(paragraphStyle, forKey: NSAttributedString.Key.paragraphStyle)
        }
        let size = CGSize(width: width, height: CGFloat(Double.greatestFiniteMagnitude))
        return ceil((self as NSString).boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attrib, context: nil).height)
    }
    
    /// Returns underlined NSAttributedString
     func underline() -> NSAttributedString {
        let underlineString = NSAttributedString(string: self, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        return underlineString
    }
    
    ///EZSE: Returns NSAttributedString
     func color(_ color: UIColor) -> NSAttributedString {
        let colorString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor: color])
        return colorString
    }
    
    ///EZSE: Returns NSAttributedString
     func colorSubString(_ subString: String, color: UIColor) -> NSMutableAttributedString {
        var start = 0
        var ranges: [NSRange] = []
        while true {
            let range = (self as NSString).range(of: subString, options: NSString.CompareOptions.literal, range: NSRange(location: start, length: (self as NSString).length - start))
            if range.location == NSNotFound {
                break
            } else {
                ranges.append(range)
                start = range.location + range.length
            }
        }
        let attrText = NSMutableAttributedString(string: self)
        for range in ranges {
            attrText.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
        return attrText
    }
    
    /// Returns if String is a number
     func isNumber() -> Bool {
        return NumberFormatter().number(from: self) != nil ? true : false
    }
    
    /// Checks if String contains Emoji
     func isIncludesEmoji() -> Bool {
        for i in 0...count {
            let c: unichar = (self as NSString).character(at: i)
            if (0xD800 <= c && c <= 0xDBFF) || (0xDC00 <= c && c <= 0xDFFF) {
                return true
            }
        }
        return false
    }
    
    /// copy string to pasteboard
     func addToPasteboard() {
        let pasteboard = UIPasteboard.general
        pasteboard.string = self
    }
    
    /// Trims white space and new line characters, returns a new string
     func trimmed() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    // URL encode a string (percent encoding special chars)
     func urlEncoded() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    // URL encode a string (percent encoding special chars) mutating version
    mutating func urlEncode() {
        self = urlEncoded()
    }
    
    // Removes percent encoding from string
     func urlDecoded() -> String {
        return removingPercentEncoding ?? self
    }
    
    /// Can be used in switch-case
     func hasPrefix(_ prefix: String) -> (_ value: String) -> Bool {
        return { (value: String) -> Bool in
            value.hasPrefix(prefix)
        }
    }
    
    /// Can be used in switch-case
     func hasSuffix(_ suffix: String) -> (_ value: String) -> Bool {
        return { (value: String) -> Bool in
            value.hasSuffix(suffix)
        }
    }
    
    /// change date formate of string
    ///
    /// - Parameters:
    ///   - currentFormate: current formate
    ///   - outputFormate: requeired formate
    /// - Returns: new string with required formate
    func convertIntoDateFormatString(currentFormate:String, outputFormate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentFormate
        let date = dateFormatter.date(from: self) ?? Date()
        dateFormatter.dateFormat = outputFormate
        return dateFormatter.string(from: date)
    }
    
    // Mutating versin of urlDecoded
    mutating func urlDecode() {
        self = urlDecoded()
    }
    
    
}

extension String {
    
    
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
}


extension String {
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func hideString() -> String{
        if self.isEmpty {
            return ""
        }
        
        let start = self.startIndex;
        let end = self.index(self.startIndex, offsetBy: self.count - 4);
        return self.replacingCharacters(in: start..<end, with: "XXXXXXXXXXX")
    }
}

extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(
            NSAttributedString.Key.strikethroughStyle,
               value: NSUnderlineStyle.single.rawValue,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
