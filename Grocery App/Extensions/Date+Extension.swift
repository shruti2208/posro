//
//  Date+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 22/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation

extension Date {
    
    /// get first day of week
    var firstDayOfWeek: Date {
        var beginningOfWeek = Date()
        var interval = TimeInterval()
        
        _ = Calendar.current.dateInterval(of: .weekOfYear, start: &beginningOfWeek, interval: &interval, for: self)
        return beginningOfWeek
    }
    
    ///Start of the Day as a date
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    /// percentage of day elapsed
//    var percentageOfDay: Double {
//        let totalSeconds = self.endOfDay.timeIntervalSince(self.startOfDay) + 1
//        let seconds = self.timeIntervalSince(self.startOfDay)
//        let percentage = seconds / totalSeconds
//        return max(min(percentage, 1.0), 0.0)
//    }
    
    /// number of weeks in a month
    var numberOfWeeksInMonth: Int {
        let calendar = Calendar.current
        let weekRange = (calendar as NSCalendar).range(of: NSCalendar.Unit.weekOfYear, in: NSCalendar.Unit.month, for: self)
        
        return weekRange.length
    }
    
    ///End of the Day as a date
//    var endOfDay: Date {
//        let cal = Calendar.current
//        var components = DateComponents()
//        components.day = 1
//        return cal.date(byAdding: components, to: self.startOfDay)!.addingTimeInterval(-1)
//    }
    
    var zeroBasedDayOfWeek: Int? {
        let comp = Calendar.current.component(.weekday, from: self)
        return comp - 1
    }
    
    /// get time string since now
    var timeAgoSinceNow: String {
        return getTimeAgoSinceNow()
    }
    
    /// get time ago since now
    /// if need localization please change the text string
    /// - Returns: time string
    private func getTimeAgoSinceNow() -> String {
        
        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " year" : "\(interval)" + " years"
        }
        
        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " month" : "\(interval)" + " months"
        }
        
        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " day" : "\(interval)" + " days"
        }
        
        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " hour" : "\(interval)" + " hours"
        }
        
        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " minute" : "\(interval)" + " minutes"
        }
        
        return "a moment ago"
    }
    
    /// count hours between two date
    ///
    /// - Parameter date: date
    /// - Returns: hours in double
    func hoursFrom(_ date: Date) -> Double {
        return Double(Calendar.current.dateComponents([.hour], from: date, to: self).hour!)
    }
    
    /// count days between dates
    ///
    /// - Parameter date: date
    /// - Returns: days in Int
    func daysBetween(_ date: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: self.startOfDay, to: date.startOfDay)
        
        return components.day!
    }
    
    func addWeeks(_ numWeeks: Int) -> Date {
        var components = DateComponents()
        components.weekOfYear = numWeeks
        
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    func weeksAgo(_ numWeeks: Int) -> Date {
        return addWeeks(-numWeeks)
    }
    
    func addDays(_ numDays: Int) -> Date {
        var components = DateComponents()
        components.day = numDays
        
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    func daysAgo(_ numDays: Int) -> Date {
        return addDays(-numDays)
    }
    
    func addHours(_ numHours: Int) -> Date {
        var components = DateComponents()
        components.hour = numHours
        
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    func hoursAgo(_ numHours: Int) -> Date {
        return addHours(-numHours)
    }
    
    func addMinutes(_ numMinutes: Double) -> Date {
        return self.addingTimeInterval(60 * numMinutes)
    }
    
    func minutesAgo(_ numMinutes: Double) -> Date {
        return addMinutes(-numMinutes)
    }
    
    /// get date string
    ///
    /// - Parameter format: formate required
    /// - Returns: string
    func stringWithFromat(format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    /// get date string in UTC
    ///
    /// - Parameter format: formate required
    /// - Returns: string
    func stringWithFromatWithUTC(format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone =  NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.string(from: self)
    }
    
    /// convert into local time
    ///
    /// - Returns: date of local time
    func toLocalTime() -> Date {
        let timeZone = NSTimeZone.local
        let seconds : TimeInterval = Double(timeZone.secondsFromGMT(for:self))
        let localDate = Date.init(timeInterval: seconds, since: self)
        return localDate
    }
    
    /// convert into local format and return string
    ///
    /// - Parameter dateFormat: format
    /// - Returns: string in required format
    func toLocalStringWithFormat(dateFormat: String) -> String {
        // change to a readable time format and change to local time zone
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
    
    /// convert date to string with format (UTC)
    ///
    /// - Parameter dateFormat: date format
    /// - Returns: new string date with required format in UTC
    func toUTCStringWithFormat(dateFormat: String) -> String {
        // change to a readable time format and change to UTC time zone
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone =  NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
   
}
