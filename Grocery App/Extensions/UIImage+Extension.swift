//
//  UIImage+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit

extension UIImage {
    
//    /// Size in bytes of UIImage
//    var bytesSize: Int {
//        return UIImage.jpegData(<#T##UIImage#>)
//        return UIImageJPEGRepresentation(self, 1)?.count ?? 0
//    }
//
//    /// Returns base64 string
//    var base64: String {
//        return UIImageJPEGRepresentation(self, 1.0)!.base64EncodedString()
//    }
    
    /// Size in kilo bytes of UIImage
//    var kilobytesSize: Int {
//        return bytesSize / 1024
//    }
    
    /// init with color and size
    ///
    /// - Parameters:
    ///   - color: color
    ///   - size: size
    convenience init(color: UIColor, size: CGSize) {
        UIGraphicsBeginImageContext(size)
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {
            self.init()
            return
        }
        
        context.setFillColor(color.cgColor)
        context.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        guard let image: UIImage = UIGraphicsGetImageFromCurrentImageContext() else {
            self.init()
            return
        }
        UIGraphicsEndImageContext()
        
        if let cgImage = image.cgImage {
            self.init(cgImage: cgImage)
        } else {
            self.init()
        }
    }
    
    /// init with tint color
    ///
    /// - Parameter color: tint color
    /// - Returns: image
    func image(withTint color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        
        guard let context: CGContext = UIGraphicsGetCurrentContext(), let cgImage = cgImage else {
            return UIImage()
        }
        
        context.scaleBy(x: 1, y: -1)
        context.translateBy(x: 0, y: -self.size.height)
        context.clip(to: rect, mask: cgImage)
        context.setFillColor(color.cgColor)
        context.fill(rect)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            return UIImage()
        }
        UIGraphicsEndImageContext()
        
        return image
    }
    
    /// Compressed UIImage data from original UIImage.
    ///
    /// - Parameter quality: The quality of the resulting JPEG image, expressed as a value from 0.0 to 1.0. The value 0.0 represents the maximum compression (or lowest quality) while the value 1.0 represents the least compression (or best quality), (default is 0.5).
    /// - Returns: optional Data (if applicable).
//    func compressedData(quality: CGFloat = 0.5) -> Data? {
//        return UIImageJPEGRepresentation(self, quality)
//    }
    
    /// Compressed UIImage from original UIImage.
    ///
    /// - Parameter quality: The quality of the resulting JPEG image, expressed as a value from 0.0 to 1.0. The value 0.0 represents the maximum compression (or lowest quality) while the value 1.0 represents the least compression (or best quality), (default is 0.5).
    /// - Returns: optional UIImage (if applicable).
//    public func compressed(quality: CGFloat = 0.5) -> UIImage? {
//        guard let data = compressedData(quality: quality) else { return nil }
//        return UIImage(data: data)
//    }
    
    /// get height as per the aspect ration
    ///
    /// - Parameter width: width
    /// - Returns: height
    public func aspectHeightForWidth(_ width: CGFloat) -> CGFloat {
        return (width * self.size.height) / self.size.width
    }
    
    
    /// get width as per the aspect ration
    ///
    /// - Parameter height: height
    /// - Returns: width
    public func aspectWidthForHeight(_ height: CGFloat) -> CGFloat {
        return (height * self.size.width) / self.size.height
    }
    /// crop to rect
    ///
    /// - Parameter rect: rect for crop
    /// - Returns: image
    func cropping(to rect: CGRect) -> UIImage? {
        let originalRect = CGRect(
            x: rect.origin.x * scale,
            y: rect.origin.y * scale,
            width: rect.size.width * scale,
            height: rect.size.height * scale
        )
        
        guard let cgImage = cgImage,
            let imageRef = cgImage.cropping(to: originalRect) else { return nil }
        
        return UIImage(cgImage: imageRef, scale: scale, orientation: imageOrientation)
    }
    
    /// resize to size
    ///
    /// - Parameter newSize: new size
    /// - Returns: image
    func resize(to newSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(CGSize(
            width: newSize.width * scale,
            height: newSize.height * scale
        ))
        
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        
        defer { UIGraphicsEndImageContext() }
        return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
    }
    
    /// resize to new size with mode
    ///
    /// - Parameters:
    ///   - newSize: new size
    ///   - scalingMode: mode, aspectfill and aspectfit
    /// - Returns: image
    func resize(to newSize: CGSize, scalingMode: ScalingMode) -> UIImage? {
        let aspectRatio = scalingMode.aspectRatio(between: newSize, and: size)
        
        let scaledImageRect = CGRect(x: (newSize.width - size.width * aspectRatio) / 2.0,
                                     y: (newSize.height - size.height * aspectRatio) / 2.0,
                                     width: size.width * aspectRatio,
                                     height: size.height * aspectRatio)
        
        UIGraphicsBeginImageContext(newSize)
        draw(in: scaledImageRect)
        defer { UIGraphicsEndImageContext() }
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    /// convert to jpg
    ///
    /// - Parameter quarity: quality
    /// - Returns: image
//    func toJPEG(quarity: CGFloat = 1.0) -> Data? {
//        return UIImageJPEGRepresentation(self, quarity)
//    }
//    
//    /// convert to png
//    ///
//    /// - Parameter quarity: quality
//    /// - Returns: image
//    func toPNG(quarity: CGFloat = 1.0) -> Data? {
//        return UIImagePNGRepresentation(self)
//    }
    
    
    
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
        
        
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
extension UIImage {
    public enum ScalingMode {
        case aspectFill
        case aspectFit
        
        func aspectRatio(between size: CGSize, and otherSize: CGSize) -> CGFloat {
            let aspectWidth  = size.width / otherSize.width
            let aspectHeight = size.height / otherSize.height
            
            switch self {
            case .aspectFill: return max(aspectWidth, aspectHeight)
            case .aspectFit: return min(aspectWidth, aspectHeight)
            }
        }
    }
}
