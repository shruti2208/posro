//
//  UINavigationController+Extension.swift
//  BankingApp
//
//  Created by Nitin on 22/09/19.
//  Copyright © 2019 Nitin. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.filter({$0.isKind(of: ofClass)}).last {
            popToViewController(vc, animated: animated)
        }
    }
}
