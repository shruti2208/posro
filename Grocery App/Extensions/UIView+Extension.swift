

import UIKit

/// Shake animations types.
///
/// - linear: linear animation.
/// - easeIn: easeIn animation.
/// - easeOut: easeOut animation.
/// - easeInOut: easeInOut animation.
public enum ShakeAnimationType {
    /// linear animation.
    case linear
    
    /// easeIn animation.
    case easeIn
    
    /// easeOut animation.
    case easeOut
    
    /// easeInOut animation.
    case easeInOut
}

public enum ShakeDirection {
    /// Shake left and right.
    case horizontal
    
    /// Shake up and down.
    case vertical
}

extension UIView {
   public func setBackgroundImage(_ named: String) {
        let image = UIImage(named: named)
        let imageView = UIImageView(frame: self.frame)
        imageView.image = image
        imageView.contentMode = .scaleToFill
        self.addSubview(imageView)
        self.sendSubviewToBack(imageView)
    }
    /// Take screenshot of view
    public var screenshot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 0)
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    /// Border color of view; also inspectable from Storyboard.
    @IBInspectable public var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            // Fix React-Native conflict issue
            guard String(describing: type(of: color)) != "__NSCFType" else { return }
            layer.borderColor = color.cgColor
        }
    }
    
    /// Border width of view; also inspec@objc table from Storyboard.
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    func setBottomBorderForView(color:UIColor) {
        //self.borderStyle = .none
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.2)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        
    }
   
     func addDropShadow(){
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.4
            layer.shadowOffset = CGSize(width: 3, height: 3)
            layer.shadowRadius = 6
            layer.cornerRadius = 21.0

        }
    func addShadow(cornerRad : CGFloat,shadowRad : CGFloat,shadowColour : CGColor) {
            layer.masksToBounds = false
            layer.shadowColor = shadowColour
            layer.shadowOpacity = 0.4
            layer.shadowOffset = CGSize(width: 3, height: 3)
            layer.shadowRadius = shadowRad
            layer.cornerRadius = cornerRad
    }
  
    /// SwifterSwift: Size of view.
    public var size: CGSize {
        get {
            return frame.size
        }
        set {
            width = newValue.width
            height = newValue.height
        }
    }
    
    /// Height of view.
    public var height: CGFloat {
        get {
            return frame.size.height
        }
        set {
            frame.size.height = newValue
        }
    }
    
    /// Width of view.
    public var width: CGFloat {
        get {
            return frame.size.width
        }
        set {
            frame.size.width = newValue
        }
    }
    
    /// left padding (leading or can say x)
    public var leftPadding: CGFloat {
        get {
            return frame.origin.x
        }
        set {
            frame.origin.x = newValue
        }
    }
    
    /// horizontal end point of view
    public var rightEnd: CGFloat {
        get {
            return leftPadding + width
        } set(value) {
            self.leftPadding = value - self.width
        }
    }
    
    /// top padding or can say y
    public var topPadding: CGFloat {
        get {
            return frame.origin.y
        }
        set {
            frame.origin.y = newValue
        }
    }
    
    /// vertical end point of view
    public var bottomEnd:CGFloat {
        
        get {
            return topPadding + height
        } set(value) {
            self.topPadding = value - height
        }
        
    }
    
    /// getter and setter for the X coordinate of the center of a view.
    public var centerX: CGFloat {
        get {
            return self.center.x
        } set(value) {
            self.center.x = value
        }
    }
    
    /// getter and setter for the Y coordinate for the center of a view.
    public var centerY: CGFloat {
        get {
            return self.center.y
        } set(value) {
            self.center.y = value
        }
    }
    
    /// Set some or all corners radiuses of view.
    ///
    /// - Parameters:
    ///   - corners: array of corners to change (example: [.bottomLeft, .topRight]).
    ///   - radius: radius for selected corners.
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
    /// Add shadow to view.
    ///
    /// - Parameters:
    ///   - color: shadow color (default is #137992).
    ///   - radius: shadow radius (default is 3).
    ///   - offset: shadow offset (default is .zero).
    ///   - opacity: shadow opacity (default is 0.5).
    //    public func addShadow(ofColor color: UIColor = UIColor(red: 0.07, green: 0.47, blue: 0.57, alpha: 1.0), radius: CGFloat = 3, offset: CGSize = .zero, opacity: Float = 0.5) {
    //        layer.shadowColor = color.cgColor
    //        layer.shadowOffset = offset
    //        layer.shadowRadius = radius
    //        layer.shadowOpacity = opacity
    //        layer.masksToBounds = false
    //    }
    
    
    /// create image from View
    ///
    /// - Returns: image
    public func toImage () -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0.0)
        drawHierarchy(in: bounds, afterScreenUpdates: false)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    /// Add array of subviews to view.
    ///
    /// - Parameter subviews: array of subviews to add to self.
    public func addSubviews(_ subviews: [UIView]) {
        subviews.forEach({ self.addSubview($0) })
    }
    
    /// Load view from nib.
    ///
    /// - Parameters:
    ///   - name: nib name.
    ///   - bundle: bundle of nib (default is nil).
    /// - Returns: optional UIView (if applicable).
    public class func loadFromNib(named name: String, bundle: Bundle? = nil) -> UIView? {
        return UINib(nibName: name, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    /// Remove all subviews in view.
    public func removeSubviews() {
        subviews.forEach({ $0.removeFromSuperview() })
    }
    
    /// Shake view.
    ///
    /// - Parameters:
    ///   - direction: shake direction (horizontal or vertical), (default is .horizontal)
    ///   - duration: animation duration in seconds (default is 1 second).
    ///   - animationType: shake animation type (default is .easeOut).
    ///   - completion: optional completion handler to run with animation finishes (default is nil).
    public func shake(direction: ShakeDirection = .horizontal, duration: TimeInterval = 1, animationType: ShakeAnimationType = .easeOut, completion:(() -> Void)? = nil) {
        CATransaction.begin()
        let animation: CAKeyframeAnimation
        switch direction {
        case .horizontal:
            animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        case .vertical:
            animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        }
        switch animationType {
        case .linear:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        case .easeIn:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        case .easeOut:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        case .easeInOut:
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        }
        CATransaction.setCompletionBlock(completion)
        animation.duration = duration
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        CATransaction.commit()
    }
    
    // resizes this view so it fits the largest subview
    public func resizeToFitSubviews() {
        var width: CGFloat = 0
        var height: CGFloat = 0
        for someView in self.subviews {
            let aView = someView
            let newWidth = aView.leftPadding + aView.width
            let newHeight = aView.topPadding + aView.height
            width = max(width, newWidth)
            height = max(height, newHeight)
        }
        frame = CGRect(x: leftPadding, y: topPadding, width: width, height: height)
    }
    
    
}
extension UIView{
    func addLightShadow() {
        let shadowSize : CGFloat = 2.5

        self.layer.cornerRadius = 8.0
        self.layer.shadowColor = UIColor.init(hex: "C0C0C0").cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 0.2
//        self.layer.masksToBounds = false;
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
        y: -shadowSize / 2,
        width: self.frame.size.width + shadowSize,
        height: self.frame.size.height + shadowSize))
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = 3

    }
}
extension UIView{
       public func addBottomShadowForView(){
           self.layer.masksToBounds = false
           self.layer.shadowColor = UIColor.init(hex: "C0C0C0").cgColor
           self.layer.shadowOpacity = 0.2
           self.layer.shadowOffset = CGSize(width: 0, height: 0)
//           self.layer.shadowRadius = 6
           self.layer.cornerRadius = 8.0
        
//        vv.layer.cornerRadius = 8.0
//            vv.layer.shadowColor = UIColor.init(hex: "C0C0C0").cgColor
//            vv.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            vv.layer.shadowOpacity = 0.2
//            vv.layer.masksToBounds = false;
//            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
//            y: -shadowSize / 2,
//            width: vv.frame.size.width + shadowSize,
//            height: vv.frame.size.height + shadowSize))
//            vv.layer.shadowPath = shadowPath.cgPath
       }
    public func addBottomShadowForViewWithRadius(cornerRad: CGFloat){
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.4
            self.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.layer.shadowRadius = 6
            self.layer.cornerRadius = cornerRad
        }
   }


