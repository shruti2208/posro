//
//  Decodable+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 22/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation

extension Decodable {
    static func decode(data: Data) throws -> Self {
        let decoder = JSONDecoder()
        return try decoder.decode(Self.self, from: data)
    }
}
