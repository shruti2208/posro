//
//  Bool+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import Foundation

extension Bool {
    /// Converts Bool to Int.
    public var toInt: Int { return self ? 1 : 0 }
    
    /// Toggle boolean value.
    @discardableResult
    public mutating func toggle() -> Bool {
        self = !self
        return self
    }
    
    /// Return inverted value of bool.
    public var toggled: Bool {
        return !self
    }
}
