//
//  UIColor+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// get image object from color
    var imageRepresentation : UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(self.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    /// init color with r,g,b
    ///
    /// - Parameters:
    ///   - r: red
    ///   - g: green
    ///   - b: blue
    convenience init(r:UInt32!, g:UInt32!, b:UInt32!) {
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:1)
    }
    
    /// init with hex, if hex is not valid it will init with white
    ///
    /// - Parameter hex: pass hex string, accept both format with # prefix and without
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    /// init with hex, if hex is not valid it will init with white
    ///
    /// - Parameter hex: pass hex string, accept both format with # prefix and without
    ///   - alpha: alpha
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = String(hex.dropFirst())
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.count) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        if r != nil && g != nil && b != nil {
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
        }
        else{
            self.init(
                red: (255),
                green: (255),
                blue: (255),
                alpha:alpha)
        }
    }
    
    
    
    /// init with hex, if hex is not valid it will init with white
    ///
    /// - Parameter hex: pass hex string, accept both format with # prefix and without
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
  
}
