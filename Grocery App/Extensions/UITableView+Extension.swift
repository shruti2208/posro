//
//  UITableView+Extension.swift
//  StarterProject
//
//  Created by Mayank Jain on 21/08/18.
//  Copyright © 2018 Mayank Jain. All rights reserved.
//

import UIKit

extension UITableView {
    
    /// Indexpath of last row
    var indexPathForLastRow: IndexPath? {
        return indexPathForLastRow(inSection: lastSection)
    }
    
    /// Index of last section in tableView.
    var lastSection: Int {
        return numberOfSections > 0 ? numberOfSections - 1 : 0
    }

    
    /// register tableview cell
    ///
    /// - Parameters:
    ///   - cellType: cell type
    ///   - bundle: bundle
    func register<T: UITableViewCell>(cellType: T.Type, bundle: Bundle = Bundle(for: T.self)) {
        let className = String(describing: cellType)
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellReuseIdentifier: className)
    }
    
    /// register multiple tableviewcell
    ///
    /// - Parameters:
    ///   - cellTypes: array of cell types
    ///   - bundle: bundle
    func register<T: UITableViewCell>(cellTypes: [T.Type], bundle: Bundle = Bundle(for: T.self)) {
        cellTypes.forEach { register(cellType: $0, bundle: bundle) }
    }
    
    /// get reusablecell
    ///
    /// - Parameters:
    ///   - type: type
    ///   - indexPath: indexpath
    /// - Returns: reusablecell
    func dequeueReusableCell<T: UITableViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: String(describing: type), for: indexPath) as! T
    }
    
    /// SwifterSwift: Number of all rows in all sections of tableView.
    ///
    /// - Returns: The count of all rows in the tableView.
    public func numberOfRows() -> Int {
        var section = 0
        var rowCount = 0
        while section < numberOfSections {
            rowCount += numberOfRows(inSection: section)
            section += 1
        }
        return rowCount
    }
    
    /// IndexPath for last row in section.
    ///
    /// - Parameter section: section to get last row in.
    /// - Returns: optional last indexPath for last row in section (if applicable).
    public func indexPathForLastRow(inSection section: Int) -> IndexPath? {
        guard section >= 0 else { return nil }
        guard numberOfRows(inSection: section) > 0  else {
            return IndexPath(row: 0, section: section)
        }
        return IndexPath(row: numberOfRows(inSection: section) - 1, section: section)
    }
    
    /// Reload data with a completion handler.
    ///
    /// - Parameter completion: completion handler to run after reloadData finishes.
    public func reloadData(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
    
    /// Scroll to bottom of TableView.
    ///
    /// - Parameter animated: set true to animate scroll (default is true).
    public func scrollToBottom(animated: Bool = true) {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height)
        setContentOffset(bottomOffset, animated: animated)
    }
    
    /// Scroll to top of TableView.
    ///
    /// - Parameter animated: set true to animate scroll (default is true).
    public func scrollToTop(animated: Bool = true) {
        setContentOffset(CGPoint.zero, animated: animated)
    }
    
    
    /// return number of rows if array count > 0 else show empty dataset
    ///
    /// - Parameters:
    ///   - dataArray: data array
    ///   - withMessage: message to show when no record
    ///   - imageName: image to show when no record
    ///   - xibName: xib to load when no record
    ///   - subTitle: subtitle to show when no record
    ///   - bgColor: background color of empty view
    /// - Returns: number of rows
    func numberOfRowsFromArray(dataArray:[Any],withMessage:String,imageName:String,xibName:String,subTitle:String,bgColor:UIColor)-> NSInteger {
        
        let tempArray = NSMutableArray.init(array: dataArray)
        if tempArray.count <= 0 {
            self.backgroundView = self.setupBackgroudViewWithNibName(name: withMessage, imageName: imageName, xibName: xibName,subTitle:subTitle)
            self.backgroundView?.backgroundColor = bgColor
            self.separatorStyle = .none
            return 0;
        }else{
            self.backgroundView=nil
        }
        return tempArray.count
    }
    
      ///Helper for emppty dataset
    private func setupBackgroudViewWithNibName(name:String,imageName:String,xibName:String,subTitle:String)-> UIView{
        
        let customView = Bundle.main.loadNibNamed(xibName, owner: self, options: nil)?[0] as! UIView
        let strImgName = imageName
        if strImgName.trimmed().count > 0 {
            (customView.viewWithTag(1001) as! UIImageView).image = UIImage.init(named: strImgName)
        }
        let strName = name

        if strName.trimmed().count > 0 {
            (customView.viewWithTag(1002) as! UILabel).text = strName
        }
        let strSubTitle = subTitle

        if strSubTitle.trimmed().count > 0 {
            (customView.viewWithTag(1003) as! UILabel).text = strSubTitle
        }else{
            (customView.viewWithTag(1003) as! UILabel).text = " "
        }
        return customView
    }
}
