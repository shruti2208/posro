//
//  Homescreen.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import Auk
import moa

class Homescreen: UIViewController,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    @IBOutlet fileprivate var heightConstraintCv: NSLayoutConstraint!
    @IBOutlet var searchBar: UISearchBar!

    @IBOutlet var cvCategory: UICollectionView!
    @IBOutlet var btnViewOffers: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var offersView: UIView!

    var categories : AllCategoriesModel!
    var banner : BannerModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        addLogoToNavigationBarItem()
        self.scrollView.delegate = self
        cvCategory.register(UINib.init(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        
        scrollView.auk.settings.contentMode = .scaleToFill
//        btnViewOffers.cornerRadius = 8
//        btnViewOffers.setBackgroundImage("offersBg")
        btnViewOffers.setBackgroundImage(UIImage(named: "offersBg"), for: .normal)
       
//        searchBar.setSearchFieldBackgroundImage(UIImage(), for: .normal)
        searchBar.backgroundColor = .white
//        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.placeholder = "Search Products"
        searchBar.setTextColor(color: Constants.AppColor.themeColorTurquoise)
        searchBar.layer.borderWidth = 0.5
        searchBar.layer.borderColor = Constants.AppColor.themeColorTurquoise.cgColor //orange
        searchBar.setPlaceholderTextColor(color: .gray)
//        searchBar.tintColor = .black
//                  searchBar.searchTextField.textColor = .black
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .black
        textField.tintColor = .black
        
        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = Constants.AppColor.themeColorTurquoise
        
        
        let clearButton = textField.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = Constants.AppColor.themeColorTurquoise
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        getBanner()
        getCategories()
        getCartDetails()
    }
    func getCartDetails(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "appVersion=\(Constants.AppIdentity.appVersion!)&API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&page_no=all"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_cart, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: CartModel = Utility.decodeData(data: result.data)
                else {
                    let model1: GenericErrorModel = Utility.decode(data: result.data)!
                    //                    print(model1.msg!)
                    //                    DispatchQueue.main.async {
                    //                        self.alert(message:(model1.msg!))
                    
                    //                    }
                    return
            }
            print(model)
            
            DispatchQueue.main.async {
                if model != nil{
                    let rightBarButton = self.navigationItem.rightBarButtonItem
                    
                    rightBarButton?.addBadge(number: (model.data?.products!.count)!)
                    
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func getBanner(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_banner, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: BannerModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.banner = model
            DispatchQueue.main.async {
                self.scrollView.auk.removeAll()

                for localImage in self.banner.data {
                    if localImage.categoryID == "0"{
                        print("Banner : ",APIEndpoint.baseImg + "assets/uploads/banner/" + localImage.image)
                        self.scrollView.auk.show(url:APIEndpoint.baseImg + "assets/uploads/banner/" + localImage.image)
                    }
                }
                self.scrollView.auk.startAutoScroll(delaySeconds: 2)
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func getCategories(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_all_categories, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AllCategoriesModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.categories = model
            DispatchQueue.main.async {
                
                self.cvCategory.reloadData()
                self.heightConstraintCv.constant = CGFloat(self.categories.data.count/2 * 170 + 150)
                self.cvCategory.layoutIfNeeded()
                
                self.cvCategory.reloadData()
                self.view.layoutIfNeeded()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if categories == nil{
            return 0
        }
        else{
            return categories.data.count
        }
    }
    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cV.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        let obj : Datum = categories.data[indexPath.row]
        cell.name.text = obj.name
//        cell.imageView.downloaded(from:"https://machotelslimited.com/posro-demo/" + "assets/uploads/category/icons/" + obj.icon)

        print(APIEndpoint.baseImg + "assets/uploads/category/banner/" + obj.icon)
//        cell.imageView.downloaded(from:APIEndpoint.baseImg + "assets/uploads/category/banner/" + obj.icon)
        cell.imageView.loadImageUsingCache(withUrl: APIEndpoint.baseImg + "assets/uploads/category/banner/" + obj.icon)
        cell.layoutIfNeeded()
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ProductsListing") as! ProductsListing
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        destinationVC.categoryId = categories.data[indexPath.row].id
        //        for items in banner.data {
        //            if categories.data[indexPath.row].id == items.id {
        destinationVC.banner = categories.data[indexPath.row].image
        //                break
        //            }
        //        }
        
        self.show(destinationVC, sender: self)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width  = (view.frame.width-20)
//        return CGSize(width: self.cvCategory.frame.width, height: 100)
        return CGSize(width: self.view.frame.width/2 - 20, height: 170)

        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ProductsListing") as! ProductsListing
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        destinationVC.searchFromHome = true
        destinationVC.searchStr = searchBar.text!
        self.show(destinationVC, sender: self)
        
    }
    @objc func didTapEditButton(sender: AnyObject) {
        print("edit")
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"Cart") as! Cart
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
       
        self.show(destinationVC, sender: self)
    }

    @objc func didTapFavoriteButton(sender: AnyObject) {
        print("favorite")
        
        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ListItems") as! ListItems
        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
  
        self.show(destinationVC, sender: self)
    }
    func addLogoToNavigationBarItem() {
        let editImage       = UIImage(named: "supermarket")!
        let favoriteImage   = UIImage(named: "editWhite")!

        let editButton      = UIBarButtonItem(image: editImage,  style: .plain, target: self, action: #selector(didTapEditButton))
        let favoriteButton  = UIBarButtonItem(image: favoriteImage,  style: .plain, target: self, action: #selector(didTapFavoriteButton))

        navigationItem.rightBarButtonItems = [editButton, favoriteButton]
        
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "logoSmall")
        //imageView.backgroundColor = .lightGray

        // In order to center the title view image no matter what buttons there are, do not set the
        // image view as title view, because it doesn't work. If there is only one button, the image
        // will not be aligned. Instead, a content view is set as title view, then the image view is
        // added as child of the content view. Finally, using constraints the image view is aligned
        // inside its parent.
        let contentView = UIView()
        self.navigationItem.titleView = contentView
        self.navigationItem.titleView?.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
}

