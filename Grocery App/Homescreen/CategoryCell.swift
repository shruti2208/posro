//
//  CategoryCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 08/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var vv: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        imageView.contentMode = .scaleAspectFit
//        imageView.contentMode = .scaleToFill
        vv.borderColor = Constants.AppColor.themeColorTurquoise
              vv.borderWidth = 1.0
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//       self.layoutIfNeeded()
//             self.imageView.contentMode = .scaleAspectFit
//        contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
//      }

}
