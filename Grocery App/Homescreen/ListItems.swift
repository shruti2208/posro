//
//  ListItems.swift
//  Grocery App
//
//  Created by Shruti Gawas on 18/07/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ListItems: UIViewController {
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var tv: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnSubmit.cornerRadius = 12
        tv.layer.cornerRadius = 6
        tv.layer.borderColor = Constants.AppColor.themeColorTurquoise.cgColor
        tv.layer.borderWidth = 0.5
        
    }
    @IBAction func submit(){
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&request_text=\("%22" + tv.text! + "%22")"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.request_tex, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: ListAddedModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    self.alert(message: "Submitted Succesfully")
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }

}
