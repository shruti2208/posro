//
//  ProductsListing.swift
//  Grocery App
//
//  Created by Shruti Gawas on 15/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import DropDown
import Auk
import moa
import ValueStepper

class ProductsListing: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate, UITextFieldDelegate {
    var searchFromHome : Bool = false
    var searchStr : String!
    @IBOutlet var searchBar: UISearchBar!
    
    var allProducts : [ProductListed] = []
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var cvCategory: UICollectionView!
    @IBOutlet var imgView: UIImageView!
    
    let unitDropdown = DropDown()
    var offers : ProductsModel!
    var categoryId : String!
    var banner : String!
    var pageCounter : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bgImage = UIImageView();
        bgImage.image = UIImage(named: "splash_screen");
        bgImage.contentMode = .scaleToFill
        cvCategory.backgroundView = bgImage
        // Do any additional setup after loading the view.
        //        print("https://machotelslimited.com/sabziwala-demo/" + "assets/uploads/category/banner/" + banner)
        //        imgView.downloaded(from:"https://machotelslimited.com/farmfresh/" + "assets/uploads/category/banner/" + banner)
        
        //        self.scrollView.delegate = self
        //        scrollView.auk.settings.contentMode = .scaleAspectFill
        
        //        self.scrollView.auk.show(url:"https://machotelslimited.com/sabziwala-demo/" + "assets/uploads/category/banner/" + banner)
        //        self.scrollView.auk.startAutoScroll(delaySeconds: 2)
        
        self.title = "Products"
        
        
        //        searchBar.setSearchFieldBackgroundImage(UIImage(), for: .normal)
        //        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.placeholder = "Search Products"
        searchBar.setTextColor(color: .black)
        searchBar.layer.borderWidth = 0.5
        searchBar.layer.borderColor = searchBar.barTintColor?.cgColor //orange
        searchBar.setPlaceholderTextColor(color: .gray)
        
        //          searchBar.searchTextField.textColor = .white
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .black
        
        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = Constants.AppColor.themeColor
        
        
        let clearButton = textField.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = .white
        
        self.cvCategory.register(UINib.init(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        
        if searchFromHome == true{
            search(searchStr: searchStr)
        }
        else{
            getProducts(pageNo: 1)
        }
        setUpDropDown()
        
    }
    func setUpDropDown() {
        // Not setting the anchor view makes the drop down centered on screen
        let tempArr : [String] = ["100 gms","250 gms","500 gms","1.0 kg","2 kg","3 kg","5 kg","10 kg"]
        
        unitDropdown.dataSource = tempArr
        unitDropdown.cellHeight = 40
        unitDropdown.separatorColor = .clear
        unitDropdown.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //        unitDropdown.selectionAction = { [weak self] (index, item) in
        //            //               self?.btnDrop.setTitle(item, for: .normal)
        //            print("selected :", self!.unitDropdown.selectedItem as Any)
        //            print("selectedIndex :", index)
        //            //            var item : Product = self!.offers.data.products[index]
        //            let indexPath = IndexPath(row: index, section: 0)
        ////            let cell: ProductCell = self!.cvCategory.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        //
        //            let cell : ProductCell = self?.cvCategory.cellForItem(at: indexPath)
        //            print(cell.lblName.text!)
        //            cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
        //            self?.cvCategory.reloadData()
        //
        //            //               self?.btnDrop.setTitleColor(UIColor.black, for: .normal)
        //        }
    }
    lazy var dropDowns: [DropDown] = {
        return [
            self.unitDropdown
        ]
    }()
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width  = (view.frame.width-20)
        return CGSize(width: self.view.frame.width, height: 150)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        if offers == nil{
        //            return 0
        //        }
        //        else{
        //            return offers.data!.products.count
        //        }
        allProducts.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == allProducts.count - 1 ) { //it's your last cell
            //Load more data & reload your collection view
            if pageCounter < offers.data!.totalPages! {
                print(pageCounter)
                pageCounter = pageCounter + 1
                getProducts(pageNo: pageCounter)
            }
        }
    }
    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cV.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell

        //        let obj : ProductListed = offers.data!.products[indexPath.row]
        let obj : ProductListed = allProducts[indexPath.row]
        
        
        if obj.localName!.count > 2{
            cell.lblName.text = obj.name! + " (" + obj.localName! + ")"
            
        }
        else{
            cell.lblName.text = obj.name!
        }
        cell.btnDrop.addTarget(self, action: #selector(showDropDown(sender:)), for: .touchUpInside)
        cell.btnAddToCart.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
        cell.btnAddToCart.tag = indexPath.row
        cell.stepper.maximumValue = obj.quantityAvailable!.doubleValue
        cell.stepper.stepValue = 1
        cell.stepper.addTarget(self, action: #selector(self.valueChanged1), for: .valueChanged)
        cell.stepper.tag = indexPath.row
        cell.lblDiscountedPrice.isHidden = true
        //        cell.stepper.isHidden = true
        
        //        if obj.quantityType == "piece"{
        //            cell.btnDrop.isHidden = true
        //            cell.lblPiece.text = obj.pieceUnit! +  " qty"
        //
        //            if obj.quantityAdded == nil{
        ////                       cell.stepper.isHidden = true
        //
        //                   }
        //                   else{
        //                       if obj.quantityAdded!.intValue > 0 {
        //                           cell.stepper.isHidden = false
        //                           cell.btnAddToCart.isHidden = true
        //                           cell.stepper.value = obj.quantityAdded!.doubleValue
        //                           cell.widthConstraint.constant = 152
        //                        cell.layoutSubviews()
        //                       }
        //                       else{
        ////                           cell.stepper.isHidden = true
        //                           cell.widthConstraint.constant = 152
        //
        //                       }
        //                   }
        //        }
        //        else{
        //            cell.btnDrop.isHidden = false
        //            cell.widthConstraint.constant = 80
        //            cell.btnDrop.setTitle(obj.quantityAdded, for: .normal)
        //            //            cell.tfQty.isHidden = true
        ////            cell.stepper.isHidden = true
        //
        //            cell.lblPiece.text = obj.weightUnit! + " kg"
        //
        //        }
        cell.btnDrop.tag = indexPath.row
        
        
        cell.lblActualPrice.text = "₹ " + obj.price!
        print(APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        //        cell.image.downloaded(from:APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        cell.image.loadImageUsingCache(withUrl: APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        cell.image.contentMode = .scaleToFill
        
        if obj.quantityAvailable == "0.000" {
            cell.widthConstraint.constant = 5
            
            cell.lblOutOfStock.isHidden = false
            cell.image.alpha = 0.5
            cell.btnAddToCart.isEnabled = false
            cell.btnDrop.isHidden = true
            cell.btnAddToCart.isHidden = true
            cell.stepper.isHidden = true
            
        }
        else{
            if obj.quantityType == "piece"{
                cell.btnDrop.isHidden = true
                cell.lblPiece.text = obj.pieceUnit! +  " qty"
                
                if obj.quantityAdded == nil{
                    cell.stepper.isHidden = true
                    
                }
                else{
                    if obj.quantityAdded!.intValue > 0 {
                        cell.stepper.isHidden = false
                        cell.stepper.setNeedsDisplay()
//                        cell.setNeedsDisplay()
                        cell.stepper.tintColor = .white
                        cell.btnAddToCart.isHidden = true
                        cell.stepper.value = obj.quantityAdded!.doubleValue
                        cell.widthConstraint.constant = 152
                    }
                    else{
                        cell.stepper.isHidden = true
                        cell.widthConstraint.constant = 152
                        
                    }
                }
            }
            else{
                cell.btnDrop.isHidden = false
                cell.widthConstraint.constant = 80
                cell.btnDrop.setTitle(obj.quantityAdded, for: .normal)
                //            cell.tfQty.isHidden = true
                cell.stepper.isHidden = true
                
                cell.lblPiece.text = obj.weightUnit! + " kg"
                
            }
            cell.lblOutOfStock.isHidden = true
            //            cell.image.alpha = 1
            //            cell.btnDrop.isEnabled = true
            //            cell.btnAddToCart.isEnabled = true
            //            cell.btnDrop.alpha = 1
            //            cell.btnAddToCart.alpha = 1
            
        }

        
        return cell
        
    }
    @objc func valueChanged1(sender: ValueStepper) {
        // Use sender.value to do whatever you want
        print(sender.tag,sender.value)
        var product : ProductListed = allProducts[sender.tag]
        
        if product.quantityAdded?.intValue != Int(sender.value) {
            addToCartFromStepper(sender: sender)
        }
    }
    @objc func showDropDown(sender: UIButton){
        unitDropdown.anchorView = sender // UIView or UIBarButtonItem
        
        unitDropdown.show()
        unitDropdown.selectionAction = { [weak self] (index, item) in
            //               self?.btnDrop.setTitle(item, for: .normal)
            print("selected :", self!.unitDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            //            var item : Product = self!.offers.data.products[index]
            //                    let indexPath = IndexPath(row: index, section: 0)
            //            let cell: ProductCell = self!.cvCategory.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
            let point: CGPoint = sender.convert(.zero, to: self?.cvCategory)
            if let indexPath = self?.cvCategory!.indexPathForItem(at: point) {
                let cell = self?.cvCategory!.cellForItem(at: indexPath) as! ProductCell
                print(cell.lblName.text!)
                cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
                
            }
            
            //                    let cell : ProductCell = self?.cvCategory.cellForItem(at: indexPath)
            //                    cell.btnDrop.setTitle(self!.unitDropdown.selectedItem!, for: .normal)
            //                    self?.cvCategory.reloadData()
            
            //               self?.btnDrop.setTitleColor(UIColor.black, for: .normal)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 123 {
            let allowingChars = "0123456789"
            let numberOnly = NSCharacterSet.init(charactersIn: allowingChars).inverted
            let validString = string.rangeOfCharacter(from: numberOnly) == nil
            return validString
        }
        return true
    }
    @objc func addToCartFromStepper(sender : ValueStepper){
        
        
        //        let obj : ProductListed = offers.data!.products[sender.tag]
        let obj : ProductListed = allProducts[sender.tag]
        
        
        //        if obj.quantityType != "piece" && {
        //
        //        }
        //                let point: CGPoint = sender.convert(.zero, to: self.cvCategory)
        //                if let indexPath = self.cvCategory!.indexPathForItem(at: point) {
        //                    let cell = self.cvCategory!.cellForItem(at: indexPath) as! ProductCell
        //                    print(cell.btnDrop.currentTitle!)
        //
        //                    if obj.quantityType != "piece" && cell.btnDrop.currentTitle == "SELECT" || obj.quantityType != "piece" && cell.btnDrop.currentTitle == "0.000"{
        //                        self.alert(message: "Select Qty")
        //                    }
        
        //            else if cell.tfQty.text!.floatValue > obj.quantityAvailable!.floatValue{
        //                self.showToast(message: "No sufficient quantity" + " Available : " + obj.quantityAvailable!)
        //
        //            }
        //            else if obj.quantityType == "piece" && cell.tfQty.text?.floatValue == 0{
        //                             self.showToast(message: "Add Qty")
        //
        //                     }
        //                    else{
        //                var qty : String!
        //                if obj.quantityType != "piece"{
        //                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
        //                    let qq : Float = Float(q)!
        //                    let qqq : Float = Float(qq / 1000)
        //                    qty = String(qqq)
        //                }
        //                else{
        //                    //                    qty = "1"
        //                    qty = cell.tfQty.text
        //
        //                }
        
        var qty : String!
        qty = String(sender.value)
        //                                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
        //                                    let qq : Float = Float(q)!
        //                                    let qUnit : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[1])!
        //                        if qUnit == "gms"{
        //                                           let qqq : Float = Float(qq / 1000)
        //                                           qty = String(qqq)
        //                                       }
        //                                       else{
        //                                           let qqq : Float = Float(qq)
        //                                           qty = String(qqq)
        //                                       }
        
        var mainArr : [NSMutableDictionary] = []
        
        var dict : NSMutableDictionary = [:]
        dict.setValue(obj.categoryID, forKey: "categoryId")
        dict.setValue(obj.cgst, forKey: "cgst")
        dict.setValue(obj.discount, forKey: "discount")
        dict.setValue(obj.id, forKey: "id")
        dict.setValue(obj.categoryID, forKey: "categoryimageUrlId")
        dict.setValue(obj.image, forKey: "imageUrl")
        dict.setValue(false, forKey: "loading")
        dict.setValue(obj.localName, forKey: "localName")
        dict.setValue(obj.name, forKey: "name")
        dict.setValue(obj.price, forKey: "price")
        dict.setValue(obj.quantityAvailable, forKey: "qtyAvailable")
        dict.setValue(obj.quantityType, forKey: "qtyType")
        dict.setValue(obj.sgst, forKey: "sgst")
        dict.setValue(obj.status, forKey: "status")
        dict.setValue(obj.price, forKey: "total")
        dict.setValue(obj.pieceUnit, forKey: "unitPiece")
        dict.setValue(obj.weightUnit, forKey: "unitWeight")
        dict.setValue(qty, forKey: "qtyAdded")
        
        mainArr.append(dict)
        
        let str = Utility.convertIntoJSONString(arrayObject: mainArr)
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&products=\(str!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                else {
                    //                            do {
                    let model1: CannotAddProductModel = Utility.decode(data: result.data)!
                    print(model1.data[0].check.msg)
                    DispatchQueue.main.async {
                        //                                self.alert(message:(model1.data[0].check.msg))
                        self.showToast(message: model1.data[0].check.msg)
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                //                        self.alert(message:(model1.data[0].check.msg))
                if model.data[0].check["qty"] == 1{
                    if model.counter == 0{
                        self.showToast(message: "Removed from Cart")
                        
                    }
                    else{
                        self.showToast(message: "Added To Cart")
                    }
                    if self.pageCounter > 1{
                        self.pageCounter = self.pageCounter - 1
                        self.getProducts(pageNo: self.pageCounter)
                    }
                    else{
                        self.allProducts.removeAll()
                        self.getProducts(pageNo: self.pageCounter)
                        
                    }
                }
                //                self.getProducts(pageNo: self.pageCounter)
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
        //                    }
        //                }
        
        
        
    }
    @objc func addToCart(sender: UIButton){
        
        //        let obj : ProductListed = offers.data!.products[sender.tag]
        let obj : ProductListed = allProducts[sender.tag]
        
        
        //        if obj.quantityType != "piece" && {
        //
        //        }
        let point: CGPoint = sender.convert(.zero, to: self.cvCategory)
        if let indexPath = self.cvCategory!.indexPathForItem(at: point) {
            let cell = self.cvCategory!.cellForItem(at: indexPath) as! ProductCell
            print(cell.btnDrop.currentTitle!)
            
            if obj.quantityType != "piece" && cell.btnDrop.currentTitle == "SELECT" || obj.quantityType != "piece" && cell.btnDrop.currentTitle == "0.000"{
                self.alert(message: "Select Qty")
            }
                
                //            else if cell.tfQty.text!.floatValue > obj.quantityAvailable!.floatValue{
                //                self.showToast(message: "No sufficient quantity" + " Available : " + obj.quantityAvailable!)
                //
                //            }
                //            else if obj.quantityType == "piece" && cell.tfQty.text?.floatValue == 0{
                //                             self.showToast(message: "Add Qty")
                //
                //                     }
            else{
                //                var qty : String!
                //                if obj.quantityType != "piece"{
                //                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
                //                    let qq : Float = Float(q)!
                //                    let qqq : Float = Float(qq / 1000)
                //                    qty = String(qqq)
                //                }
                //                else{
                //                    //                    qty = "1"
                //                    qty = cell.tfQty.text
                //
                //                }
                
                var qty : String!
                if obj.quantityType != "piece"{
                    
                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
                    let qq : Float = Float(q)!
                    let qUnit : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[1])!
                    if qUnit == "gms"{
                        let qqq : Float = Float(qq / 1000)
                        qty = String(qqq)
                    }
                    else{
                        let qqq : Float = Float(qq)
                        qty = String(qqq)
                    }
                }
                else{
                    qty = "1"
                    
                }
                var mainArr : [NSMutableDictionary] = []
                
                var dict : NSMutableDictionary = [:]
                dict.setValue(obj.categoryID, forKey: "categoryId")
                dict.setValue(obj.cgst, forKey: "cgst")
                dict.setValue(obj.discount, forKey: "discount")
                dict.setValue(obj.id, forKey: "id")
                dict.setValue(obj.categoryID, forKey: "categoryimageUrlId")
                dict.setValue(obj.image, forKey: "imageUrl")
                dict.setValue(false, forKey: "loading")
                dict.setValue(obj.localName, forKey: "localName")
                dict.setValue(obj.name, forKey: "name")
                dict.setValue(obj.price, forKey: "price")
                dict.setValue(obj.quantityAvailable, forKey: "qtyAvailable")
                dict.setValue(obj.quantityType, forKey: "qtyType")
                dict.setValue(obj.sgst, forKey: "sgst")
                dict.setValue(obj.status, forKey: "status")
                dict.setValue(obj.price, forKey: "total")
                dict.setValue(obj.pieceUnit, forKey: "unitPiece")
                dict.setValue(obj.weightUnit, forKey: "unitWeight")
                dict.setValue(qty, forKey: "qtyAdded")
                
                mainArr.append(dict)
                
                let str = Utility.convertIntoJSONString(arrayObject: mainArr)
                
                let message: [String: Any] = [:]
                
                let manager = WSManager.shared
                let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&products=\(str!)"
                
                let postData =  parameters.data(using: .utf8)
                
                
                manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
                    
                    
                    
                    guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                        else {
                            //                            do {
                            let model1: CannotAddProductModel = Utility.decode(data: result.data)!
                            print(model1.data[0].check.msg)
                            DispatchQueue.main.async {
                                //                                self.alert(message:(model1.data[0].check.msg))
                                self.showToast(message: model1.data[0].check.msg)
                            }
                            return
                    }
                    print(model)
                    DispatchQueue.main.async {
                        //                        self.alert(message:(model1.data[0].check.msg))
                        //                        if model.data[0].check["qty"] == 1{
                        if model.status == true{
                            self.showToast(message: "Added To Cart")
                            //                            self.getProducts(pageNo: self.pageCounter)
                            if self.pageCounter > 1{
                                self.pageCounter = self.pageCounter - 1
                                self.getProducts(pageNo: self.pageCounter)
                            }
                            else{
                                self.allProducts.removeAll()
                                self.getProducts(pageNo: self.pageCounter)
                                
                            }
                        }
                    }
                }) { (error) in
                    
                    
                    Utility.handleNetworkError(error: error)
                    
                }
            }
        }
        
        
    }
    func search(searchStr : String){
        offers = nil
        allProducts.removeAll()
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&term=\(searchStr)&page_no=1"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_product_search, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: ProductsModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            if model.status == true{
                self.offers = model
                self.allProducts.append(contentsOf: model.data!.products)
            }
            
            
            DispatchQueue.main.async {
                
                self.cvCategory.reloadData()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func getProducts(pageNo : Int){
        if pageNo == 1{
            allProducts.removeAll()
        }
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        //        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&page_no=\(pageNo)&category_id=\(categoryId!)&appVersion=\(Constants.AppIdentity.appVersion!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_products, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: ProductsModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            if model.code == 200{
                self.offers = model
                self.allProducts.append(contentsOf: model.data!.products)
                DispatchQueue.main.async {
                    
                    self.cvCategory.reloadData()
                   
                }
            }
            else{
                self.showToast(message: "No Products Found")
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            getProducts(pageNo: 1)
            cvCategory.reloadData()
        }
        //        else{
        //            search(searchStr: searchText)
        //        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            getProducts(pageNo: 1)
        }
        else{
            search(searchStr: searchBar.text!)
        }
    }
    
}
