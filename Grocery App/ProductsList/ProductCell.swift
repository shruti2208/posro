//
//  ProductCell.swift
//  Grocery App
//
//  Created by Shruti Gawas on 08/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import ValueStepper
class ProductCell: UICollectionViewCell {
    @IBOutlet var vv: UIView!
//    @IBOutlet var tfQty: UITextField!
    @IBOutlet var stepper: ValueStepper!

    @IBOutlet var btnDrop: UIButton!
    @IBOutlet var btnAddToCart: UIButton!
    
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var lblActualPrice: UILabel!
    @IBOutlet var lblOutOfStock: UILabel!
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var lblPiece: UILabel!
    @IBOutlet var lblDiscountedPrice: UILabel!
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnDrop.layer.borderWidth = 0.5
        btnDrop.layer.borderColor =  Constants.AppColor.themeColorGray.cgColor
        
        vv.layer.borderWidth = 0.5
        vv.layer.borderColor =  Constants.AppColor.themeColor.cgColor
//        self.stepper.isHidden = true

    }
    override func prepareForReuse() {
        // invoke superclass implementation
        super.prepareForReuse()
        
        // reset (hide) the checkmark label

//        self.stepper.isHidden = true
        btnAddToCart.isHidden = false
        self.lblOutOfStock.isHidden = true
        self.btnDrop.isHidden = true
        image.alpha = 1

        btnDrop.isEnabled = false
        btnAddToCart.isEnabled = false
        btnDrop.alpha = 1
        btnAddToCart.alpha = 1
        image.contentMode = .scaleAspectFit
        widthConstraint.constant = 5

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
    }
}
