//
//  ForgotPassword.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    @IBOutlet weak var vUsername: UIView!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        vUsername.addBottomShadowForView()
        btnSend.cornerRadius = 12
        
    }
    func otp(){
        let message : [String: Any] = [:]
        let parameters = "API_KEY=FabCodersENCRYPT&mobile=\(tfUsername.text!)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&social=0"
        let postData =  parameters.data(using: .utf8)
        let manager = WSManager.shared
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.send_otp, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            guard let model: OtpModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                if model.status == true{
                    let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"VerifyOTP") as! VerifyOTP
                    destinationVC.phone = self.tfUsername.text!
                    destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.show(destinationVC, sender: self)
                }
            }
            
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @IBAction func back(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendOtp(){
        if tfUsername.text!.isEmpty {
            self.alert(message: "Enter Phone Number")
        }
        else if Utility.validatePhoneNumber(phoneNumber: tfUsername.text!) == false{
            self.alert(message: "Enter Valid Phone Number")
            
        }
        else{
            let refreshAlert = UIAlertController(title: "SEND OTP", message: "Are you sure you want to send OTP on " + tfUsername.text!, preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "SEND", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle SEND logic here")
                self.otp()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
            
        }
    }
}
