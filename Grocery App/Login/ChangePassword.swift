//
//  ChangePassword.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class ChangePassword: UIViewController {
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var confirmPass: UITextField!
    
    @IBOutlet weak var vNewPass: UIView!
    @IBOutlet weak var vConfirmPass: UIView!
    @IBOutlet weak var btnVerify: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        vNewPass.addBottomShadowForView()
        vConfirmPass.addBottomShadowForView()

        btnVerify.cornerRadius = 12
    }
   @IBAction func changePassword(){
        if newPass.text!.isEmpty {
            self.alert(message: "Enter New Password")
        }
        else if confirmPass.text!.isEmpty {
            self.alert(message: "Confirm New Password")
        }
        else if confirmPass.text! != newPass.text!{
            self.alert(message: "Passwords dont match")
        }
            
        else{
            
        }
    }
}
