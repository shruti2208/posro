//
//  Login.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class Login: UIViewController {
    @IBOutlet weak var vUsername: UIView!
    @IBOutlet weak var vPass: UIView!
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setBackgroundImage("loginBg")
        vUsername.addBottomShadowForView()
        vPass.addBottomShadowForView()
//        tfUsername.setBottomBorder(color: .white)
//        tfPass.setBottomBorder(color: .white)
//        tfUsername.setPlaceHolderTextColor(.white)
//        tfPass.setPlaceHolderTextColor(.white)

        tfUsername.setUnderLine()
        tfPass.setUnderLine()

        btnLogin.cornerRadius = 12
        
//        btnForgotPass.setAttributedTitle(partialUnderline(regular: "Forgot Password ? ", underlined: "Touch Here"), for: .normal)
        btnForgotPass.halfTextColorChangeTitle(fullText: "Forgot Password ? Touch Here", changeText: "Touch Here")
        btnSignUp.halfTextColorChangeTitle(fullText: "Don't have an account? Sign up", changeText: "Sign up")

//        btnSignUp.setAttributedTitle(partialUnderline(regular: "Don't have an account? ", underlined: "Sign up"), for: .normal)
        
    }
    
    func partialUnderline(regular : String,underlined : String) -> NSAttributedString{
        let underlineAttribute : [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 16),
            .foregroundColor: UIColor.black,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let regularAttribute : [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.black,
            .underlineStyle: 0]
        
        let underlineText = NSAttributedString(string: regular, attributes: regularAttribute)
        let regularText = NSAttributedString(string: underlined, attributes: underlineAttribute)
        let newString = NSMutableAttributedString()
        newString.append(underlineText)
        newString.append(regularText)
        
        return newString
    }
    func validate() {
        
        if tfUsername.text!.isEmpty {
            self.alert(message: "Enter Phone Number")
        }
        else if tfPass.text!.isEmpty{
            self.alert(message: "Enter Password")
            
        }
        else if Utility.validatePhoneNumber(phoneNumber: tfUsername.text!) == false{
            self.alert(message: "Enter Valid Phone Number")
            
        }
        else{
            login()
        }
        
    }
    @IBAction func loginClicked(){
        validate()
    }
    func login(){
        let message : [String: Any] = [:]
        let parameters = "user_name=\(tfUsername.text!)&password=\(tfPass.text!)&appVersion=\(Constants.AppIdentity.appVersion!)&API_KEY=FabCodersENCRYPT"
        let postData =  parameters.data(using: .utf8)
        let manager = WSManager.shared
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.login, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            guard let model: LoginModel = Utility.decodeData(data: result.data)
                else {
                    let model1: UpdatedContactErrorModel = Utility.decode(data: result.data)!
                    print(model1.data)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.data))
                        
                    }
                    return
            }
            print(model)
            let defaults = UserDefaults.standard
            
            defaults.set(true, forKey: "loggedIn")
            defaults.set(model.data.name, forKey: "userName")
            defaults.set(model.data.email, forKey: "email")
            defaults.set(model.data.contact, forKey: "phone")
            defaults.set(model.data.id, forKey: "userId")

            defaults.set(true, forKey: "loggedIn")

            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(model) {
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "userInfo")

            }
    
            DispatchQueue.main.async {
                if model.status == true {
                    let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"NavController") as! NavController
                    destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                    self.show(destinationVC, sender: self)
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    @IBAction func forgotPass(){
          let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ForgotPassNav") as! ForgotPassNav
          destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
          self.show(destinationVC, sender: self)
      }
}
