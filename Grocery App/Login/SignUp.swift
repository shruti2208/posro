//
//  SignUp.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class SignUp: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var vUsername: UIView!
    @IBOutlet weak var vEmail: UIView!
    @IBOutlet weak var vMobile: UIView!
    @IBOutlet weak var vPass: UIView!
    @IBOutlet weak var vConfirmPass: UIView!
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var tfConfirmPass: UITextField!
    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setBackgroundImage("loginBg")

        vUsername.addBottomShadowForView()
        vEmail.addBottomShadowForView()
        vMobile.addBottomShadowForView()
        vPass.addBottomShadowForView()
        vConfirmPass.addBottomShadowForView()

        tfPass.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        tfConfirmPass.attributedPlaceholder = NSAttributedString(string: "Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        tfMobile.delegate = self
        
        btnSignup.cornerRadius = 12
        btnLogin.halfTextColorChangeTitle(fullText: "Already have an account? Log in", changeText: "Log in")
        btnTerms.setAttributedTitle(partialUnderline(regular: "By clicking Sign up you agree to our Terms and Conditions", underlined: " Terms and Conditions"), for: .normal)
    }
  func partialUnderline(regular : String,underlined : String) -> NSAttributedString{
        let underlineAttribute : [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 16),
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let regularAttribute : [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.black,
            .underlineStyle: 0]
        
        let underlineText = NSAttributedString(string: regular, attributes: regularAttribute)
        let regularText = NSAttributedString(string: underlined, attributes: underlineAttribute)
        let newString = NSMutableAttributedString()
        newString.append(underlineText)
        newString.append(regularText)
        
        return newString
    }
    @IBAction func register(){
        if validate() == true{
            let message: [String: Any] = [:]
            
            let manager = WSManager.shared
            let parameters = "API_KEY=FabCodersENCRYPT&name=\(tfUsername.text!)&email=\(tfEmail.text!)&mobile=\(tfMobile.text!)&password\(tfPass.text!)"
            
            let postData =  parameters.data(using: .utf8)
            
            
            manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.signup, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
                
                
                
                guard let model: LoginModel = Utility.decodeData(data: result.data)
                    else {
                        let model1: GenericError = Utility.decode(data: result.data)!
                        print(model1.message)
                        DispatchQueue.main.async {
                            self.alert(message:(model1.message))
                            
                        }
                        return
                }
                print(model)
                        let defaults = UserDefaults.standard
                        
                        defaults.set(true, forKey: "loggedIn")
                        defaults.set(model.data.name, forKey: "userName")
                        defaults.set(model.data.email, forKey: "email")
                        defaults.set(model.data.contact, forKey: "phone")
                        defaults.set(model.data.id, forKey: "userId")

                        defaults.set(true, forKey: "loggedIn")

                        let encoder = JSONEncoder()
                        if let encoded = try? encoder.encode(model) {
                            let defaults = UserDefaults.standard
                            defaults.set(encoded, forKey: "userInfo")

                        }
                
                        DispatchQueue.main.async {
                            if model.status == true {
                                let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"NavController") as! NavController
                                destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                                self.show(destinationVC, sender: self)
                            }
                        }
            }) { (error) in
                
                
                Utility.handleNetworkError(error: error)
                
            }
        }
    }
 
    func validate()-> Bool{
        var valid : Bool = false
        if tfUsername.text!.isEmpty || tfEmail.text!.isEmpty || tfPass.text!.isEmpty || tfConfirmPass.text!.isEmpty || tfMobile.text!.isEmpty{
        Utility.getTopMostViewController()?.showToast(message: "All Fields are mandatory")
        }
        else if tfPass.text! != tfConfirmPass.text!{
            Utility.getTopMostViewController()?.showToast(message: "Passwords do not match")
        }
        else if Utility.validateEmailId(emailID: tfEmail.text!) == false{
            Utility.getTopMostViewController()?.showToast(message: "Enter valid Email")

        }
        else if Utility.validatePhoneNumber(phoneNumber: tfMobile.text!) == false{
            Utility.getTopMostViewController()?.showToast(message: "Enter valid Phone")

        }
        else{
            valid = true
        }
        return valid
    }
//    {API_KEY=FabCodersENCRYPT, name=Shruti G, email=shrutig@fabcoders.in, mobile=8208517484, password=123456}
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool
    {
        if textField == tfMobile{
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 10
        }
        else{
            return true
        }
    }
}
