//
//  VerifyOTP.swift
//  Grocery App
//
//  Created by Shruti Gawas on 05/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit

class VerifyOTP: UIViewController {
    @IBOutlet weak var tfOtp: UITextField!
    @IBOutlet weak var vOtp: UIView!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var topLbl: UILabel!

    var phone : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        vOtp.addBottomShadowForView()
        
        btnVerify.cornerRadius = 12
        topLbl.text = "OTP has been sent to " + phone
    }
   @IBAction func verifyOtp(){
        if tfOtp.text!.isEmpty {
            self.alert(message: "Enter OTP")
        }
        else{
            let message : [String: Any] = [:]
            let parameters = "API_KEY=FabCodersENCRYPT&mobile=\(phone!)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&social=0&otp=\(tfOtp.text!)"
            let postData =  parameters.data(using: .utf8)
            let manager = WSManager.shared
            
            manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.verify_otp, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
                
                guard let model: VerifiedOtpModel = Utility.decodeData(data: result.data)
                    else {
                        let model1: ErrorModel = Utility.decode(data: result.data)!
                        print(model1.msg!)
                        DispatchQueue.main.async {
                            self.alert(message:(model1.msg!))
                            
                        }
                        return
                }
                print(model)
                DispatchQueue.main.async {
                    if model.status == true{
                        let destinationVC = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier:"ChangePassword") as! ChangePassword
                        destinationVC.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                        self.show(destinationVC, sender: self)
                    }
                }
                
            }) { (error) in
                
                
                Utility.handleNetworkError(error: error)
                
            }
        }
    }
    
}
