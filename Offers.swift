//
//  Offers.swift
//  Grocery App
//
//  Created by Shruti Gawas on 08/06/20.
//  Copyright © 2020 Fabcoders. All rights reserved.
//

import UIKit
import DropDown
import ValueStepper

class Offers: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate{
    var allProducts : [ProductListed] = []
    var pageCounter : Int = 1
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var cvCategory: UICollectionView!
    var offers : OffersModel!
    let unitDropdown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Offers"
        let bgImage = UIImageView();
        bgImage.image = UIImage(named: "splash_screen");
        bgImage.contentMode = .scaleToFill
        cvCategory.backgroundView = bgImage
        
        
        //        searchBar.setSearchFieldBackgroundImage(UIImage(), for: .normal)
        //        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.placeholder = "Search Products"
        searchBar.setTextColor(color: .black)
        searchBar.layer.borderWidth = 0.5
        searchBar.layer.borderColor = searchBar.barTintColor?.cgColor //orange
        searchBar.setPlaceholderTextColor(color: .gray)
        //        searchBar.barTintColor = .white
        //        searchBar.searchTextField.textColor = .white
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .black
        
        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = Constants.AppColor.themeColor
        
        
        let clearButton = textField.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = .white
        
        cvCategory.register(UINib.init(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        getOffers(pageNo: 1)
        setUpDropDown()
    }
    func setUpDropDown() {
        // Not setting the anchor view makes the drop down centered on screen
        let tempArr : [String] = ["100 gms","250 gms","500 gms","1.0 kg","2 kg","3 kg","5 kg","10 kg"]
        
        unitDropdown.dataSource = tempArr
        unitDropdown.cellHeight = 40
        unitDropdown.separatorColor = .clear
        unitDropdown.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        unitDropdown.selectionAction = { [weak self] (index, item) in
            //               self?.btnDrop.setTitle(item, for: .normal)
            print("selected :", self!.unitDropdown.selectedItem as Any)
            print("selectedIndex :", index)
            //            var item : Product = self!.offers.data.products[index]
            let indexPath = IndexPath(row: index, section: 0)
            let cell: ProductCell = self!.cvCategory.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
            cell.btnDrop.setTitle(self!.unitDropdown.selectedItem, for: .normal)
            self?.cvCategory.reloadData()
            
            //               self?.btnDrop.setTitleColor(UIColor.black, for: .normal)
        }
    }
    lazy var dropDowns: [DropDown] = {
        return [
            self.unitDropdown
        ]
    }()
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == allProducts.count - 1 ) { //it's your last cell
            //Load more data & reload your collection view
            if pageCounter < offers.data!.totalPages! {
                print(pageCounter)
                pageCounter = pageCounter + 1
                getOffers(pageNo: pageCounter)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width  = (view.frame.width-20)
        return CGSize(width: self.view.frame.width, height: 120)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if offers == nil{
            return 0
        }
        else{
            return offers.data!.products.count
        }
    }
    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cV.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        //        let obj : ProductListed = offers.data!.products[indexPath.row]
        let obj : Product = offers.data!.products[indexPath.row]
        
        if obj.localName!.count > 2{
            cell.lblName.text = obj.name! + " (" + obj.localName! + ")"
            
        }
        else{
            cell.lblName.text = obj.name!
        }
        cell.lblDiscountedPrice.text = "₹ " + obj.discount!
        cell.btnDrop.addTarget(self, action: #selector(showDropDown(sender:)), for: .touchUpInside)
        cell.btnAddToCart.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
        cell.btnAddToCart.tag = indexPath.row
        //        cell.tfQty.tag = 123
        //        cell.tfQty.delegate = self
        cell.stepper.maximumValue = obj.quantityAvailable!.doubleValue
        cell.stepper.stepValue = 1
        cell.stepper.addTarget(self, action: #selector(self.valueChanged1), for: .valueChanged)
        cell.stepper.tag = indexPath.row
        let str = "MRP : ₹ " + obj.price!
        cell.lblActualPrice.attributedText = str.strikeThrough()
        
        if obj.quantityAdded == nil{
            cell.stepper.isHidden = true
            
        }
        else{
            if obj.quantityAdded!.intValue > 0 {
                cell.btnAddToCart.isHidden = true
                cell.stepper.isHidden = false
                cell.stepper.setNeedsDisplay()
                cell.setNeedsDisplay()
                cell.stepper.value = obj.quantityAdded!.doubleValue
                cell.widthConstraint.constant = 152
                
            }
            else{
                cell.stepper.isHidden = true
                cell.widthConstraint.constant = 152
                
            }
        }
        if obj.quantityType == "piece"{
            cell.btnDrop.isHidden = true
            //            cell.tfQty.isHidden = false
            //            cell.tfQty.text = obj.quantityAdded
            //            cell.stepper.isHidden = false
            
            cell.lblPiece.text = obj.pieceUnit! +  " qty"
        }
        else{
            cell.btnDrop.isHidden = false
            cell.btnDrop.setTitle(obj.quantityAdded, for: .normal)
            //            cell.tfQty.isHidden = true
            //            cell.stepper.isHidden = true
            cell.widthConstraint.constant = 80
            
            cell.lblPiece.text = obj.weightUnit! + " kg"
            
        }
        cell.btnDrop.tag = indexPath.row
        
        
        //        cell.lblActualPrice.text = "₹ " + obj.price!
        print(APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        //        cell.image.downloaded(from:APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        cell.image.loadImageUsingCache(withUrl: APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
        cell.image.contentMode = .scaleToFill
        
        if obj.quantityAvailable == "0.000" {
            cell.lblOutOfStock.isHidden = false
            cell.image.alpha = 0.5
            cell.btnDrop.isEnabled = false
            //            cell.btnAddToCart.isEnabled = false
            //            cell.btnDrop.alpha = 0.5
            //            cell.btnAddToCart.alpha = 0.5
            cell.btnDrop.isHidden = true
            cell.btnAddToCart.isHidden = true
            cell.stepper.isHidden = true
            cell.widthConstraint.constant = 5
            
        }
        else{
            
            cell.lblOutOfStock.isHidden = true
            cell.image.alpha = 1
            cell.btnDrop.isEnabled = true
            cell.btnAddToCart.isEnabled = true
            cell.btnDrop.alpha = 1
            cell.btnAddToCart.alpha = 1
            
        }
        return cell
        
    }
    @objc func valueChanged1(sender: ValueStepper) {
        // Use sender.value to do whatever you want
        print(sender.tag,sender.value)
        //        var product : ProductListed = allProducts[sender.tag]
        let product : Product = offers.data!.products[sender.tag]
        if product.quantityAdded?.intValue != Int(sender.value) {
            addToCartFromStepper(sender: sender)
        }
    }
    @objc func addToCartFromStepper(sender : ValueStepper){
        
        
        //        let obj : ProductListed = offers.data!.products[sender.tag]
        let obj : ProductListed = allProducts[sender.tag]
        
        
        //        if obj.quantityType != "piece" && {
        //
        //        }
        //                let point: CGPoint = sender.convert(.zero, to: self.cvCategory)
        //                if let indexPath = self.cvCategory!.indexPathForItem(at: point) {
        //                    let cell = self.cvCategory!.cellForItem(at: indexPath) as! ProductCell
        //                    print(cell.btnDrop.currentTitle!)
        //
        //                    if obj.quantityType != "piece" && cell.btnDrop.currentTitle == "SELECT" || obj.quantityType != "piece" && cell.btnDrop.currentTitle == "0.000"{
        //                        self.alert(message: "Select Qty")
        //                    }
        
        //            else if cell.tfQty.text!.floatValue > obj.quantityAvailable!.floatValue{
        //                self.showToast(message: "No sufficient quantity" + " Available : " + obj.quantityAvailable!)
        //
        //            }
        //            else if obj.quantityType == "piece" && cell.tfQty.text?.floatValue == 0{
        //                             self.showToast(message: "Add Qty")
        //
        //                     }
        //                    else{
        //                var qty : String!
        //                if obj.quantityType != "piece"{
        //                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
        //                    let qq : Float = Float(q)!
        //                    let qqq : Float = Float(qq / 1000)
        //                    qty = String(qqq)
        //                }
        //                else{
        //                    //                    qty = "1"
        //                    qty = cell.tfQty.text
        //
        //                }
        
        var qty : String!
        qty = String(sender.value)
        //                                    let q : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[0])!
        //                                    let qq : Float = Float(q)!
        //                                    let qUnit : String = (cell.btnDrop.currentTitle?.components(separatedBy: " ")[1])!
        //                        if qUnit == "gms"{
        //                                           let qqq : Float = Float(qq / 1000)
        //                                           qty = String(qqq)
        //                                       }
        //                                       else{
        //                                           let qqq : Float = Float(qq)
        //                                           qty = String(qqq)
        //                                       }
        
        var mainArr : [NSMutableDictionary] = []
        
        var dict : NSMutableDictionary = [:]
        dict.setValue(obj.categoryID, forKey: "categoryId")
        dict.setValue(obj.cgst, forKey: "cgst")
        dict.setValue(obj.discount, forKey: "discount")
        dict.setValue(obj.id, forKey: "id")
        dict.setValue(obj.categoryID, forKey: "categoryimageUrlId")
        dict.setValue(obj.image, forKey: "imageUrl")
        dict.setValue(false, forKey: "loading")
        dict.setValue(obj.localName, forKey: "localName")
        dict.setValue(obj.name, forKey: "name")
        dict.setValue(obj.price, forKey: "price")
        dict.setValue(obj.quantityAvailable, forKey: "qtyAvailable")
        dict.setValue(obj.quantityType, forKey: "qtyType")
        dict.setValue(obj.sgst, forKey: "sgst")
        dict.setValue(obj.status, forKey: "status")
        dict.setValue(obj.price, forKey: "total")
        dict.setValue(obj.pieceUnit, forKey: "unitPiece")
        dict.setValue(obj.weightUnit, forKey: "unitWeight")
        dict.setValue(qty, forKey: "qtyAdded")
        
        mainArr.append(dict)
        
        let str = Utility.convertIntoJSONString(arrayObject: mainArr)
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&products=\(str!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                else {
                    //                            do {
                    let model1: CannotAddProductModel = Utility.decode(data: result.data)!
                    print(model1.data[0].check.msg)
                    DispatchQueue.main.async {
                        //                                self.alert(message:(model1.data[0].check.msg))
                        self.showToast(message: model1.data[0].check.msg)
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                //                        self.alert(message:(model1.data[0].check.msg))
                if model.data[0].check["qty"] == 1{
                    self.showToast(message: "Added To Cart")
                    self.showToast(message: "Added To Cart")
                    if self.pageCounter > 1{
                        self.pageCounter = self.pageCounter - 1
                        self.getOffers(pageNo: self.pageCounter)
                    }
                    else{
                        self.allProducts.removeAll()
                        self.getOffers(pageNo: self.pageCounter)
                        
                    }
                }
                //                self.getProducts(pageNo: self.pageCounter)
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
        //                    }
        //                }
        
        
        
    }
    //    func collectionView(_ cV: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //        let cell = cV.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
    //        let obj : Product = offers.data!.products[indexPath.row]
    //        //        cell.mainView.borderColor = Constants.AppColor.themeColorGray
    //        //        cell.mainView.borderWidth = 1
    //        cell.lblName.text = obj.name
    ////        cell.lblDiscountedPrice.text = "₹ " + obj.discount!
    //        cell.lblActualPrice.text = "₹ " + obj.discount!
    //        cell.btnDrop.addTarget(self, action: #selector(showDropDown(sender:)), for: .touchUpInside)
    //        cell.btnAddToCart.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
    //        cell.btnAddToCart.tag = indexPath.row
    //
    //        cell.btnDrop.tag = indexPath.row
    //        let str = "MRP : ₹ " + obj.price!
    //        cell.lblActualPrice.attributedText = str.strikeThrough()
    //        print(APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
    //        cell.image.downloaded(from:APIEndpoint.baseImg + "assets/uploads/" + obj.image!)
    //        if obj.quantityType == "piece"{
    //                cell.btnDrop.isHidden = true
    ////                cell.tfQty.isHidden = false
    ////                cell.tfQty.text = obj.quantityAdded
    //                cell.lblPiece.text = obj.pieceUnit! +  " qty"
    //            }
    //            else{
    //                cell.btnDrop.isHidden = false
    ////                cell.btnDrop.setTitle(obj.quantityAdded, for: .normal)
    ////                cell.tfQty.isHidden = true
    //                cell.lblPiece.text = obj.weightUnit! + " kg"
    //
    //            }
    //
    //        if obj.quantityAvailable == "0.000" {
    //            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    //            label.center = cell.vv.center
    //            label.numberOfLines = 0 //If you want to display only 2 lines replace 0(Zero) with 2.
    //            label.lineBreakMode = .byWordWrapping //Word Wrap
    //            // OR
    //            //            lbl.lineBreakMode = .byCharWrapping //Charactor Wrap
    //            label.textAlignment = .center
    //            label.textColor = .red
    ////            label.text = "OUT OF STOCK"
    //            //            label.backgroundColor = .white
    //            cell.lblOutOfStock.isHidden = false
    //
    //            cell.image.alpha = 0.5
    //            cell.addSubview(label)
    //
    //            cell.btnDrop.isEnabled = false
    //            cell.btnAddToCart.isEnabled = false
    //            cell.btnDrop.alpha = 0.5
    //            cell.btnAddToCart.alpha = 0.5
    //
    //
    //        }
    //        else{
    //            cell.lblOutOfStock.isHidden = true
    //
    //        }
    //        return cell
    //
    //    }
    @objc func showDropDown(sender: UIButton){
        unitDropdown.anchorView = sender // UIView or UIBarButtonItem
        unitDropdown.show()
    }
    @objc func addToCart(sender: UIButton){
        let obj : Product = offers.data!.products[sender.tag]
        var mainArr : [NSMutableDictionary] = []
        
        var dict : NSMutableDictionary = [:]
        dict.setValue(obj.categoryID, forKey: "categoryId")
        dict.setValue(obj.cgst, forKey: "cgst")
        dict.setValue(obj.discount, forKey: "discount")
        dict.setValue(obj.id, forKey: "id")
        dict.setValue(obj.categoryID, forKey: "categoryimageUrlId")
        dict.setValue(obj.image, forKey: "imageUrl")
        dict.setValue(false, forKey: "loading")
        dict.setValue(obj.localName, forKey: "localName")
        dict.setValue(obj.name, forKey: "name")
        dict.setValue(obj.price, forKey: "price")
        dict.setValue(obj.quantityAvailable, forKey: "qtyAvailable")
        dict.setValue(obj.quantityType, forKey: "qtyType")
        dict.setValue(obj.sgst, forKey: "sgst")
        dict.setValue(obj.status, forKey: "status")
        dict.setValue(obj.price, forKey: "total")
        dict.setValue(obj.pieceUnit, forKey: "unitPiece")
        dict.setValue(obj.weightUnit, forKey: "unitWeight")
        dict.setValue(1.0, forKey: "qtyAdded")
        
        mainArr.append(dict)
        
        let str = Utility.convertIntoJSONString(arrayObject: mainArr)
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&products=\(str!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.update_cart_product, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: AddedToCartModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            DispatchQueue.main.async {
                
                if model.data[0].check["qty"] == 1{
                    self.showToast(message: "Added To Cart")
                    if self.pageCounter > 1{
                        self.pageCounter = self.pageCounter - 1
                        self.getOffers(pageNo: self.pageCounter)
                    }
                    else{
                        self.allProducts.removeAll()
                        self.getOffers(pageNo: self.pageCounter)
                        
                    }
                }
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func getOffers(pageNo:Int){
        
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&page_no=\(pageNo)&user_id=\(UserDefaults.standard.string(forKey: "userId")!)"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_products_with_discount, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: OffersModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.offers = model
            DispatchQueue.main.async {
                
                self.cvCategory.reloadData()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func search(searchStr : String){
        offers = nil
        let message: [String: Any] = [:]
        
        let manager = WSManager.shared
        let parameters = "API_KEY=FabCodersENCRYPT&user_id=\(UserDefaults.standard.string(forKey: "userId")!)&term=\(searchStr)&page_no=1"
        
        let postData =  parameters.data(using: .utf8)
        
        
        manager.requestWSCallUrlEncoded(strWSName: APIEndpoint.get_product_search, method: .post, accessToken: "", params: message, body: postData!, isShowLoader: true, checksum: "", success: { (result) in
            
            
            
            guard let model: OffersModel = Utility.decodeData(data: result.data)
                else {
                    let model1: ErrorModel = Utility.decode(data: result.data)!
                    print(model1.msg!)
                    DispatchQueue.main.async {
                        self.alert(message:(model1.msg!))
                        
                    }
                    return
            }
            print(model)
            self.offers = model
            DispatchQueue.main.async {
                
                self.cvCategory.reloadData()
            }
        }) { (error) in
            
            
            Utility.handleNetworkError(error: error)
            
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            getOffers(pageNo: 1)
            cvCategory.reloadData()
        }
        //        else{
        //            search(searchStr: searchText)
        //        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            getOffers(pageNo: 1)
        }
        else{
            search(searchStr: searchBar.text!)
        }
    }
}
//MARK: - UISearchBar EXTENSION
extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func getSearchBarTextField() -> UITextField? {
        return getViewElement(type: UITextField.self)
    }
    
    func setTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.textColor = color
        }
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 6
            case .prominent, .default:
                textField.backgroundColor = color
            }
        }
    }
    
    func setPlaceholderTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor: color])
        }
    }
    
    func setTextFieldClearButtonColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            
            let button = textField.value(forKey: "clearButton") as! UIButton
            if let image = button.imageView?.image {
                button.setImage(image.transform(withNewColor: color), for: .normal)
            }
        }
    }
    
    func setSearchImageColor(color: UIColor) {
        
        if let imageView = getSearchBarTextField()?.leftView as? UIImageView {
            imageView.image = imageView.image?.transform(withNewColor: color)
        }
    }
}



extension UIImage {
    
    func transform(withNewColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

